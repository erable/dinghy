#!/usr/bin/env python
##  Contributors : 
##  Laurent Bulteau
##  Alice Julien-Laferriere
##  Delphine Parrot
##  
##  l.bulteau@gmail.com
##
##This software is a computer program whose purpose is to extract an json file from 
## a list of reactions and a sbml file in order to visualize it afterward.
#This software is governed by the CeCILL license under French law and
##abiding by the rules of distribution of free software.  You can  use, 
##modify and/ or redistribute the software under the terms of the CeCILL
##license as circulated by CEA, CNRS and INRIA at the following URL
##"http://www.cecill.info". 
## 
##As a counterpart to the access to the source code and  rights to copy,
##modify and redistribute granted by the license, users are provided only
##with a limited warranty  and the software's author,  the holder of the
##economic rights,  and the successive licensors  have only  limited
##liability. 
##
##In this respect, the user's attention is drawn to the risks associated
##with loading,  using,  modifying and/or developing or reproducing the
##software by the user in light of its specific status of free software,
##that may mean  that it is complicated to manipulate,  and  that  also
##therefore means  that it is reserved for developers  and  experienced
##professionals having in-depth computer knowledge. Users are therefore
##encouraged to load and test the software's suitability as regards their
##requirements in conditions enabling the security of their systems and/or 
##data to be ensured and,  more generally, to use and operate it in the 
##same conditions as regards security. 
##
##The fact that you are presently reading this means that you have had
##knowledge of the CeCILL license and that you accept its terms.

from libsbml import *
from collections import Counter # Used for the Cofactor
import re

# Returns
## reactionsXML  = dict() # id : reactionName, ensuite tupple de lists ((reactif), (produits), reversible)
## usedMetabolites = Counter  : number of time each metabolite is used
## oldLines = list() : lines precedding the list of reactions and species...
def readSBML(  fileName, reactionsList, cofactorsList): 
## reactionsList  :set of reactions allowed
    reader = SBMLReader()
    mySBMLDoc = reader.readSBML(fileName)   
    errors = mySBMLDoc.getNumErrors() # not necessary
    print(" validation error(s): " + str(errors));
    network = mySBMLDoc.getModel()

## Reading the compartiments:
    compartiments =dict()    
    try : ## Nb: in EColi this is not define, it should be... In karp example we DO have a compartiments LIst
        allCompartiments = network.getListOfCompartiments()
        gotCom = True
        for comp in allCompartiments :
         compartiments [comp.getId()] = compartiments.getName()
    except: # no compartiment list
        gotCom = False 


## end read compartiments

    allSpecies  = network.getListOfSpecies();
    metabolitesCounter = Counter()#
#    print "the network contains " + str(allSpecies.size()) + " species and " +  str(allReactions.size()) + " reactions \n" 
    ## Reading Species

    allMetabolites = dict() ## TODO : to use replacing usedMetabolites
    for specie in allSpecies:
        ## NB: formula does not seem to be a regular field in xml
        allAtt = dict()
        allAtt["charge"] = specie.getCharge()
        allAtt["name"] = specie.getName()
        currentComp = specie.getCompartment()
        allAtt["compartment"] =  currentComp
        if not gotCom:
            compartiments[currentComp] = currentComp


        if specie.getNotes() :
                notes = specie.getNotesString()
                attributesIt = re.finditer('(?<=<p>)[^,</p]+', notes)                
                for att in attributesIt:
                    splitted =  att.group(0).split(":")
                    allAtt[splitted[0]] =  splitted[1]
        allMetabolites[specie.getId()] = allAtt
 ## reading reactions      
    allReactions = network.getListOfReactions();
    reactionsXML = dict() # id : reactionId, ensuite tupple de lists ((reactif), (produits), reversible)
    usedMetabolites = set() ## to select on
    cofactorMetabolites = set()
    for  reaction in allReactions : ## enumerating over the reactions 
        if reaction.getId() in reactionsList:
            reversible = reaction.getReversible()
            products = map( lambda x : (x.getSpecies(), x.getStoichiometry() ), reaction.getListOfProducts() )# with stochiometry, name as string, double as stochio
            reactants = map( lambda x : (x.getSpecies(), x.getStoichiometry() ),reaction.getListOfReactants() )
       ## Handling cofactor
            for prod in products :
                if prod[0] in cofactorsList:
                    metabolitesCounter+= Counter({prod[0]})#increment
                    thisMetabolite = "CF"+  str(metabolitesCounter[prod[0]])+"_"+prod[0]
                    cofactorMetabolites.add(thisMetabolite)
                    products[ products.index(prod) ] = (thisMetabolite, prod[1])
                    allMetabolites[thisMetabolite] = allMetabolites[prod[0]]
            for react in reactants :
                if react[0] in cofactorsList:
                    metabolitesCounter+= Counter({react[0]})#increment
                    thisMetabolite = "CF"+  str(metabolitesCounter[react[0]])+"_"+react[0]
                    cofactorMetabolites.add(thisMetabolite)
                    reactants[ reactants.index(react)] = (thisMetabolite, react[1])
                    allMetabolites[thisMetabolite] = allMetabolites[react[0]]
         ## Finding subsystem if indicated
            subsystem = "Unknown"
            if reaction.getNotes() :
                notes = reaction.getNotesString()
                if "SUBSYSTEM" in notes:
                        subsystem=re.search('(?<=SUBSYSTEM: )[^\<]+', notes).group(0)
            usedMetabolites |=  set(map(lambda x : x[0], products)) ## do the union of the first element (name)
            usedMetabolites |=  set(map(lambda x : x[0], reactants)) ## do the union 
            reactionsXML[reaction.getId()] = ( reactants, products , reversible, subsystem, reaction.getName() )
            ## returning what is needed afterward
    ## intersect usedMetabolites and allMetabolites
    metaWithNotes = dict([(k,allMetabolites[k]) for k in usedMetabolites ])# if k in allMetabolites])
    ## remove the cofactores
    for cofacto in cofactorsList:
        #allMetabolites.pop(cofacto, None)
        errorCo = allMetabolites.pop(cofacto, 0)  
        if errorCo==0:
            print cofacto + " was not found in xml file"
 #  return metaWithNotes  
    return (reactionsXML,metaWithNotes, cofactorMetabolites, compartiments)

#This software is governed by the CeCILL license under French law and

import os 
import re
import sys
import argparse #python Licence
import csv # GNu licence
import json #writting with indentation ! whouhou

from collections import Counter # Used for the Cofactor

def main():
    extractSubXML_INSTDIR=os.path.dirname(os.path.abspath(sys.argv[0]))


    # ========================================================================
    #                        Manage command line arguments
    # ========================================================================
    parser = argparse.ArgumentParser(description='extractSubXml') #to redo
    
    # ------------------------------------------------------------------------
    #                            Define allowed options
    # ------------------------------------------------------------------------
    # Mandatory
    versionNumber = "0.0.1"
    parser.add_argument('OriginalXML', action="store", help="Original XML")
    parser.add_argument('selectedReactions', action="store", help="selected reactions")
    #Facultative
    parser.add_argument('--cofactors', action = 'store',dest = 'cofactorsFile', default = False, required = False, help = "")
    parser.add_argument('--direction', action = 'store_true', dest = 'direction', default = False, required = False, 
                        help = "Boolean, if used the first character of every reactions should indicate the direction taken ( <, >, #)")
    parser.add_argument('--values', action = 'store_true',  dest = 'readValues', default = False, required = False,
                        help = 'If option used, reactions values in second column of input file will be used') 
    parser.add_argument('--csv', action = 'store', type=bool, dest = 'csv', default = False, required = False,
                        help = 'output the informations extracted from the xml into a csv file') 
    parser.add_argument('--version', action='version', version='%(prog)s' + versionNumber)  
    parser.add_argument('--bn', action = 'store', dest = 'blackNodes', default = "", required=False,
                        help = 'File containing the list of black nodes')   
 
    # ------------------------------------------------------------------------
    #               Parse and interpret command line arguments
    # ------------------------------------------------------------------------
    options = parser.parse_args()           
    if not options.OriginalXML:
        parser.error('[error] An xml file  is required')
    reference = options.OriginalXML 

    if not options.selectedReactions:
        parser.error('[error] A selectedReactions file is required')
    selectedReactions = options.selectedReactions 
    prefixHyper =  ".".join(re.split( "\.", selectedReactions )[:-1]  )
    if prefixHyper=="":#if the reactions file does not have extention
        prefixHyper = selectedReactions

    cofactors = set()
    if options.cofactorsFile:
        f = open(options.cofactorsFile, "r")    
        for line in f:
            cofactors.add(line.split()[0])
        f.close()
 ########### Reading the file of reactions
    reactionsLIST = set()
    if options.readValues :
        reactionValue = {}
    reactionOrientation = {}

    f = open(selectedReactions, "r") # add try,
    for line in f : # one line / reaction
        emptyLine =False
        if options.direction: # we use the first 
            begin = 1
            orientation = line[0]
        else :
            begin = 0
        if options.readValues :
            thisLine = line[begin:].split()
            if len(thisLine) > 1 : # otherwise empty line
                reactionName = thisLine[0] 
                reactionValue[reactionName] = thisLine[1] 
            else :
                if len(thisLine) == 0:
                    emptyLine = True
                else:
                    print "error while reading reactions file with values see line : " + line
                    sys.exit()
        else :
            thisLine = line[begin:].split()
            if len(thisLine)>0 : 
                reactionName = thisLine[0] 
            else :
                emptyLine = True
        if not emptyLine:
            reactionsLIST.add( reactionName )
            if options.direction :
                reactionOrientation[ reactionName ] = orientation 
    f.close()
    blackNodes = set()
    if options.blackNodes != "":
       f = open( options.blackNodes)
       for line in f:
            l=line.split()
            if len(l)>0: 
               blackNodes.add(l[0])
       f.close()
       print "BlackNodes : "+str(len(blackNodes))+" found"

    lectureOutput =  readSBML( reference, reactionsLIST, cofactors )   
    reactionsXML  = lectureOutput[0] # id : reactionId, ensuite tupple de lists ((reactif), (produits), reversible)
    usedMetabolites = lectureOutput[1]
    cofactorMetabolites = lectureOutput[2]
    compartiments = lectureOutput[3]


    if not options.direction:
        for reactionName, informations in reactionsXML.iteritems():
            if(reactionsXML[reactionName][2]):
                reactionOrientation[reactionName] = "#"
            else:
                reactionOrientation[reactionName] = ">"   
 

    if options.csv:
        c = csv.writer( open( "reactions.csv", "wb"))
        for reaction in reactionsLIST:
            sub = reactionsXML [reaction][3]      
            nameR =  reactionsXML [reaction][4]
            if options.readValues:
                c.writerow( [reaction , nameR, reactionValue[reaction], sub] )
            else : 
                c.writerow( [reaction , nameR, sub] )

  ##### Writing the JSON ###############   





    countingmap={}  
    counter=0

    linksDict = []
    nodesDict = []# array of dicts 
    subsystems = set()
        # Visit Metabolites
    for shortName, attributes in usedMetabolites.iteritems():
        ## Filling the dictionnary
        newMeta = {}
        newMeta["id"] = shortName
        newMeta["shortname"] = shortName
        newMeta["name"] = attributes["name"]
        newMeta["group"] = "metabolite"
        newMeta["cofactor"] = int( shortName in cofactorMetabolites )
        newMeta["compartment"] = attributes["compartment"] 
        newMeta["charge"] = attributes["charge"]
        newMeta["blacknode"] = int( shortName in blackNodes )
        # adding it to the nodes
        nodesDict += [newMeta]
# Creates the ids used for the list afterwards
        countingmap[shortName]=counter
        counter=counter+1
    # Visit Reactions
    for reaction in reactionsLIST:
        if not reactionsXML.has_key(reaction):
            print "pb with the reaction: \"" + reaction + ", not found in the reference xml file\""
        else:
            newReactNode = {}
           
            newReactNode["id"] =reaction 
            newReactNode["name"] =  reaction
            newReactNode["direction"]= reactionsXML[reaction][2] ## Reversibility         
            newReactNode ["orientation"]  = reactionOrientation[reaction]
            if options.readValues:
                newReactNode["value"] = reactionValue[reaction]   
            newReactNode["subsystem"]= reactionsXML[reaction][3]
            subsystems.add( reactionsXML[reaction][3] )
            newReactNode["group"] = "reaction"
            nodesDict += [newReactNode]
             
            ## Updating the linklist
           

            for metabolite in reactionsXML[reaction][0]:
                newLink = {}
                newLink["source"] = countingmap[metabolite[0]]
                newLink["target"] = counter
                newLink["name"] = reaction
                newLink["subsystem"] = reactionsXML[reaction][3]
                if options.readValues:
                   newLink["value"] = reactionValue[reaction]                 
                newLink["orientation"] = reactionOrientation[reaction]       
                linksDict += [newLink] ## one link by substrate   
                
            for metabolite in reactionsXML[reaction][1]:
                newLink = {}
                newLink["source"] = counter
                newLink["target"] = countingmap[metabolite[0]]
                newLink["name"] = reaction
                newLink["subsystem"] = reactionsXML[reaction][3]
                if options.readValues:
                   newLink["value"] = reactionValue[reaction]                 
                newLink["orientation"] = reactionOrientation[reaction]
                linksDict += [newLink] ## one link by product    
            counter=counter+ 1 # moving on to the next reaction
        # end reactions
    ## Coloring the pathways
    systemsDict = []
    for system in subsystems:
        color="#"
        color += hex((hash(system)% 160) +256 )[-2:]
        color += hex((hash(system)/160% 160) +256)[-2:]
        color += hex((hash(system)/160/160% 160) +256)[-2:]
        systemsDict +=  [{ "id": system, "color": color }]
        
 ## Using the compartiments       
    comparDict= []
    for idKey in compartiments.keys():
        comparDict += [{ "id" : idKey, "name " : compartiments[idKey]}]

    print "writing in " + prefixHyper +".json "
    with open( prefixHyper + ".json", "w") as jsonP:
        allData = { "version": [{"number" : versionNumber}], "nodes" : nodesDict,"links" : linksDict, "compartiments" : comparDict,"systems" : systemsDict}
        json.dump(allData, jsonP, indent=2)
        #    json.dump({}, jsonP, indent=2)
         #   json.dump({"nodes" : nodesDict}, jsonP, indent=2)
          #  json.dump({"links" : linksDict}, jsonP, indent=2)
           # json.dump({"compartiments" : comparDict}, jsonP, indent=2)
           # json.dump({"subsystem" : systemsDict}, jsonP, indent=2)

  #  print systemsDict
        

if __name__ == '__main__':
    main()
