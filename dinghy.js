

//!
//!  Parameters
//!

/**
rawData={"text":"a->b+c\nc->d+e\ne->f\nf->g\na+f->e\nh->i\n h+u->i\n i->j \n j->k\n k->l\n"}
 /**
 rawParameters={"rounds":"1", "coeffGravity":"0.7", "coeffCharge":"1", "coeffArcDistance":"5", "coeffReactionRepulsion":"1", "arcUnitThickness":"2", "arcMinThickness":"1", "arcMaxThickness":"20", "arcCurveScale":"1", "nodeScale":"1.4", "separateComponents":"1", "trimIdRight":"0", "trimIdLeft":"2", "chargeDistance":"", "useNodeVoronoi":true, "useEgdeVoronoi":false, "showCofactors":false, }
 /**/
 
 
//verbose:
// O = none
// 1 = main steps
// 2 = start/end main functions (heavy with force system)
// 3+ = specific debug
//   6 : click & selections
//   7 : unusual exceptions
//   8 : split and join nodes
verbose=[1,7];


parameters= {
   data: "example.json",
   rounds : 1,
   useNodeVoronoi:1,
   useEdgeVoronoi:0,
   voronoiThreshold:40, //minimum node width for the voronoi to be active
   shadowThreshold:40, //minimum node width for the link shadows to be visible
   coeffGravity : 0.5,
   coeffGravityFar : 3, //max 5 // increase gravity beyond a "comfort" distance => avoid stupidly big graphs
   nodeComfortArea : 3, //a node should have room for several equal nodes on average
   coeffCharge : 1, //TODO 1
   coeffArcDistance : 1, 
   coeffReactionRepulsion:1,
   filterRatio : 0,   
   compartmentPlacement:1,
   compartmentColor:0,
   fluidity:4,
   arcUnitThickness : 4,// 0,
   arcMinThickness : 1,//-0.5,
   arcMaxThickness : 13,// 3, 
   arcCurveScale:1,
   nodeScale:1.4,
   separateComponents:1,
   trimIdRight:0,
   trimIdLeft:0,
   showCofactors:true,
   demo:0,
   enableToolTipBox:0,
   useLastLetterAsCompartment:true,
   strongOrientationAngle:4, 
   strongOrientationStrength:0.7,
	gatherSelection:false,
	selectionDrag:true,
   orientTopDown:0.17,
   oppositeSignOppositeDirection:true,
	autoLabel:1,
	showLabelName:true,
	showLabelId:false,
	showLabelShortName:false,
	showLabelCompartment:false
}



function readQuery(qs) {
    qs = qs.split("+").join(" ");
    
    var tokens,
        re = /[?&]?([^=]+)=([^&#]*)/g;

    while (tokens = re.exec(qs)) {
        parameters[decodeURIComponent(tokens[1])]
            = decodeURIComponent(tokens[2]);
    }
}

readQuery(document.location.search);


var setterFunction={};
function mapParameter(label, type, callBack) {
	var id="choose"+capitalizeFirstLetter(label);
	
	if (type == "check") {
		d3.select("#"+id).on("change", function() {			
			parameters[label] = this.checked;   				
			if (callBack) callBack();
		})		
		setterFunction[label]= function(newValue) {
			d3.select("#"+id).node().checked = newValue;
		}
	} else if (type == "radio" || type=="radioNumber") {
		d3.selectAll("."+id).on("change", function() {			
			log("set Param " + label,5);
			console.log("xx");
			if ( type=="radioNumber") {
				parameters[label] = +d3.selectAll("."+id+":checked").attr("value");
			} else {
				parameters[label] = d3.selectAll("."+id+":checked").attr("value");
			}
			if (callBack) callBack();
		})				
		setterFunction[label]= function(newValue) {
			d3.selectAll("."+id).each(function(x) {
					if (this.value == newValue) this.checked=true;
			})
		}
	} else if (type == "input" || type=="inputNumber") {
		d3.select("#"+id).on("change", function() {			
			if (type=="inputNumber") 
				parameters[label] = +this.value;   				
			else
				parameters[label] = this.value;   			
			if (callBack) callBack();
		})					
		setterFunction[label]= function(newValue) {
			d3.select("#"+id).node().value=newValue;
		};
	}	
	setterFunction[label](parameters[label])
}


mapParameter("strongOrientationAngle","radioNumber",updateStrongOrientation);
mapParameter("showCofactors","check",updateShowCofactors);
mapParameter("filterRatio","inputNumber",updateFilter);
mapParameter("fluidity","radioNumber",updateFixed);
mapParameter("gatherSelection","check",updateGathering);
mapParameter("compartmentPlacement","check",updateCompartmentParameters);
mapParameter("compartmentColor","check",updateCompartmentParameters);
mapParameter("selectionDrag","check");
mapParameter("coeffGravity","inputNumber",updateDynamicsCoeff);
mapParameter("coeffCharge","inputNumber",updateDynamicsCoeff);
mapParameter("orientTopDown","inputNumber",updateStrongOrientation);
mapParameter("nodeScale","inputNumber",updateNodeView);
mapParameter("coeffArcDistance","inputNumber",updateArcParameters);
mapParameter("oppositeSignOppositeDirection","check",updateNodeView);


mapParameter("autoLabel","radioNumber",updateNodeLabels);
mapParameter("showLabelName","check",updateNodeLabels);
mapParameter("showLabelId","check",updateNodeLabels);
mapParameter("showLabelShortName","check",updateNodeLabels);
mapParameter("showLabelCompartment","check",updateNodeLabels);
mapParameter("trimIdLeft","inputNumber",updateNodeLabels);
mapParameter("trimIdRight","inputNumber",updateNodeLabels);


//max distance for "local" operations
var locality=3;


//data gathered
var data; //object containing the whole data
var node; //node objects
var metabolitenode;
var reactionnode;
var link; //link objects
var selection=[];
var selnode=d3.select(); //selection overlay objects

//var systems; //=data.systems
//var systemsArray; 
var systemBox

//spaned and zoomed container
var container;

var maxValue=0;

var forceOn = 2;  
var tickcount=0;
var nloops=0;
var enableViewUpdate = true;

var medianReactionValue = 1;

var nNodes;
var trackZoom = [];


/* 
TODO
   read stoichiometry
   show node values  
  
	import SBML
	split hypergraph / rest
	zoom: 1, fit, +, -  
*/


/* (this is obsolete)
 data.node = array of objects with keys:
  - id : metabolite/reaction identifier (string)
  - name : printable name
  - group : "metabolite" or "reaction"
 (reactions only) 
  - subsystemID : identifier of subsystem for this reaction
  - subsystem : printable name of subsystem 
  - subsystem : printable name of subsystem 
  - value : cplex output
  - direction : boolean "true"/"false"
  - cofactor : 0/1 (contains only cofactor metabolites)
  - orientation : 
     > (default, or ignore)
	 < show arrow the wrong way
	 # show double arrow
 (metabolites only)
  - cofactor : 0/1
  - blacknode : 0/1
 (created in javascript)
  - index : index of the node in the array
  - adjacencyList : array of neighbors
  - distance : array: d -> list of vertices at distance d
  - connectcomponent : index of the CC
  - value (for metabolites) : max abs. value of a neighbor
  
  data.links = array of objects with keys:
  - source : index of the source
  - target : index of the target 
  - name/subsystem/subsystemID/value/direction : redundant with reaction
  (created in javascript)
  - outward : boolean  
  
  
  */ 
   
   
   
   
   
   
   /*************** PART 1  *************/
   /*    Graph functions                */
   /*    Node and link classification   */
   /*                                   */
   /*************************************/   

//!
//! Utilities
//!

function sLongList(l) {
   return l.length>1?"s":"";
}

function idsToNodes(s) {
   if (typeof s == "object") return findNodes(s);
   return data.nodes[s];
}
function nodesToIds(s) {
   if ("length" in s) return s.map(get("id"));
   return s.id;
}
function shallowCopy(original) {
   var copy={};
   Object.keys(original).forEach(function(x){ 
      copy[x] = original[x];      
   })  
   return copy;
}
function uniq(a) {
   if (a && typeof a[0]=="object") {
      return a.sort(function(x,y){return x.id-y.id}).filter(function(item, pos) {
        return !pos || item != a[pos - 1];
      })
   }
    return a.sort().filter(function(item, pos) {
        return !pos || item != a[pos - 1];
    })
}
function get(field, subfield) {
	if (typeof subfield=="undefined") 
	return function(x) {
		if (x) return x[field];
	}
	else 
	return function(x) {
		if (x && x[field]) 
		return x[field][subfield];
	}
}
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function log(x,i) {
	if (typeof i=="undefined" ) {
		i=3;
	}
	if (verbose.indexOf(i)>=0) {
      if (i==1) 
         console.log(x);
      else {
         if (typeof x != "object") 
            console.log("["+i+"] "+x);
         else {
            console.log("["+i+"] ");
            console.log(x);
         }         
      }
	}
	
}


//!
//!
//!

function exportNode() {
	this.raw.x=this.x;
	this.raw.y=this.y;
   if (this.split)
      this.raw.split=[this.split[0], nodesToIds(this.split[1])];
	return this.raw;
}
function exportLink() {
   this.raw.source=this.source.id;
   this.raw.target=this.target.id;
	return this.raw;
}
function exportSystem() {
	return this.raw;
}
function exportCompartment() {
	return this.raw;
}
function exportGroup() {
   var z=shallowCopy(this);
	z.nodes = z.nodes.map(get("id"));
	return z;
}

function importGroup(g) {
   var z=shallowCopy(g);
	z.nodes = findNodes(z.nodes);
   z.toJSON = exportGroup;
	return z;
}		
function importSingleRawObject(keep, exportFunction) {
   return function (n) {
      var z= {raw:n,
         toJSON : exportFunction}
      keep.forEach(function(key) {
         if (key in n) z[key] = n[key];
      })
      return z;
   }
}	
function importRawData() {
	
	data.nodes = data.nodes.map(importSingleRawObject(["x", "y"], exportNode));
	data.links = data.links.map(importSingleRawObject([], exportLink));	
	if (data.systems) data.systems = data.systems.map(importSingleRawObject(["name","color","inFilter"], exportSystem));	
	if (data.compartments) data.compartments = data.compartments.map(importSingleRawObject(["color","center"], exportCompartment));	
	if (data.rigidGroups) data.rigidGroups = data.rigidGroups.map(importGroup);	
}


//!
//!  ids to node data
//!

function findNodes(ids) {
   return ids.map(function(x) {
      return data.nodes[x];
   });   
}


function metabolite(n) {
//   return n.group=="metabolite" || n.group =="node"|| n.group =="vertex";
	return n.group==0;
}
function metaboliteNotCof(n) {
	return n.group==0 &&!n.cofactor;
}
function metaboliteCof(n) {
	return n.group==0 && n.cofactor;
}
function cofactor(n) {
   return n.cofactor;
}
function reaction(n) {
//   return n.group=="reaction" || n.group =="edge" || n.group=="hyperedge"|| n.group =="arc" || n.group=="hyperarc";
   return n.group==1;
}
function inFilter(n) {
	return n.inFilter;
}



//! list all reactions objects with ids in given list, sort by value
function findReactions(ids) {
   var list=[];
   ids.forEach(function(x) {
      var n = data.nodes[x];
      if (reaction(n) ) {
         list.push(n);         
      }
   });   
   list.sort(function(a,b){
      if (Math.abs(a.value)>Math.abs(b.value)) 
         return -1;
      else 
         return 1;
   })
   return list;
}



//!
//!  neighborhoods
//!




function neighborhood() {

   data.nodes.forEach(function(n, i) { 
      n.adjacencyList=[];
      n.adjacentLinks=[];
      n.inAdjacencyList=[];
      n.outAdjacencyList=[];
      
      n.distance=[];   	
	   n.nextStep=[]; // = out adjacency list, but taking into account reversible reactions
   });

   data.links.forEach(function(l, i) { 
      l.source.adjacencyList.push(l.target.mainIndex);
      l.target.adjacencyList.push(l.source.mainIndex);
      l.source.outAdjacencyList.push(l.target.mainIndex);
      l.target.inAdjacencyList.push(l.source.mainIndex);  
      
      l.source.adjacentLinks.push(l);
      l.target.adjacentLinks.push(l);
	  
   });

   data.nodes.forEach(function(n, i) { 
      n.degree = n.adjacencyList.length;
      n.distance[1]=n.adjacencyList;
      var l=n.adjacencyList;
      n.inAdjacencyList.forEach(function (x) {
         l=l.concat(data.nodes[x].inAdjacencyList)
      })
      n.outAdjacencyList.forEach(function (x) {
         l=l.concat(data.nodes[x].outAdjacencyList)
      })
      n.extendedAdjacencyList = l.filter(function(x) {return (!data.nodes[x].cofactor)}); 	  
   });
   
   for (var i=2; i<=locality; i++) {
      
		data.nodes.forEach(function(d) {          
			var l = d.distance[i-1];
			d.adjacencyList.forEach(function(x) {
				l = l.concat(data.nodes[x].distance[i-1])
			});
			d.distance[i]=uniq( l );         
		});      
   }     
   
   log("Neighborhood: ok",1);
}


//!
//! connected components
//!

var CC={count:0, center : [], sample: [], size:[], filterSize:[]};

function VisitNeighbors(nodes, start, CCId) {
   var thisCC=0;
   var stack=[start];
   while (stack.length>0) {
	  thisCC++;
      i=stack.pop();
	 
      if (nodes[i].connectedcomponent<0) {
		  nodes[i].adjacencyList.forEach(function(n){
			if (nodes[n].ccdepth<=0) { 
				nodes[n].ccdepth = nodes[i].ccdepth+1 
			}
			stack.unshift(n);
		  });
          nodes[i].connectedcomponent = CCId;          
      } else {
		 nloops++;
	  }
   }
   CC.size[CCId] = thisCC; 
   CC.filterSize[CCId] = thisCC;
   CC.sample[CCId] = start;   
   
}


function computeCCs() {
	 CC.count=0;
    data.nodes.forEach(function(d, i) { 
	    d.ccdepth=0;
       d.connectedcomponent=-1;
    });
    
    data.nodes.forEach(function(d, i) { 
       if (d.connectedcomponent==-1) {
            
	       VisitNeighbors(data.nodes, i, CC.count);
          CC.count++;
	    }
    });    
    
    

   for (i=0; i<CC.count; i++) {
      CC.center.push({
           "x": Math.cos((i+0.5)/CC.count*6.28)*50*(Math.sqrt(nNodes)*0.2+Math.sqrt(CC.size[i]))*parameters.separateComponents,
           "y": Math.sin((i+0.5)/CC.count*6.28)*50*(Math.sqrt(nNodes)*0.2+Math.sqrt(CC.size[i]))*parameters.separateComponents
         })
   }
    
   log("CCs: ok",2);
}


//!
//!  outsiders
//!

// outsider = degree 1 after removing cofactors

function computeOutsiders() {
   
   data.nodes.forEach(function(d, i) { 
      var degree=0;
      findNodes(d.adjacencyList).forEach( function(n) {
         if (! (metabolite(n) && n.cofactor == 1)) 
            degree++
      }); 
      if (degree<=1) {
         d.outsider=1;
         findNodes(d.adjacencyList).forEach( function(n) {
            n.nearOutsider=1;
         }); 
      }       
   });    
}


//!
//! First Data Read
//!


function checkData() {
   data.nodes.forEach( function(n) {
	  n.keep=0;
   })
   data.links.forEach( function(l) {
	  l.source=l.raw.source;
	  l.target=l.raw.target;
      if (typeof l.source != "object") l.source=data.nodes[l.source];
      if (typeof l.target != "object") l.target=data.nodes[l.target];
   })
   
   
   data.nodes.forEach(function(n,i) {n.mainIndex=i; n.id=i; });

}


//!
//!   Node functions
//! 

 
function defGroup(n) {
	if (! ("group" in n.raw)) {
	    throwError("group must be defined for every node")
	} else {
	    g=n.raw.group.toLowerCase();
		n.group=-1;
		if (g=="metabolite" || g=="node" || g=="vertex") {
		  n.group=0;
		}
		if (g=="reaction" || g=="edge" || g=="arc" || g=="hyperedge" || g=="hyperarc") {
		  n.group=1;
		}
		n.groupName=capitalizeFirstLetter(g);		
	}
}
function defShortName(n) {
	if ("shortName" in n.raw) {
		n.shortName=n.raw.shortname;
	} else if ("shortname" in n.raw) {
		n.shortName=n.raw.shortname;
	} else if ("id" in n.raw) {
		n.shortName=n.raw.id;
	} else if ("name" in n.raw) {
		n.shortName=n.raw.name;
	} else  {
		n.shortName=n.groupName +"_"+n.mainIndex;
	}
}
 
function defName(n) {
	if ("name" in n.raw) {
		n.name=n.raw.name;
	} else {
		n.name=n.shortName;
	}
}
 
function defSearchFields(n) {
	n.searchFields=[];
	["name", "id", "shortName", "shortname", "keyword", "EC", "keywords"].forEach(function(k) {
		if (k in n.raw) {
			if (typeof n.raw[k] == "array") 
				n.searchFields= n.searchFields.concat((n.raw[k]+"").toUpperCase());
			else
				n.searchFields.push((n.raw[k]+"").toUpperCase());
		}
	})
}

function defBlackNode(n) {
	if ("blacknode" in n.raw) {
		n.blacknode=(n.raw.blacknode?true:false)
	} else {
		n.blacknode=false;
	}
}
function defCofactor(n) {
    //must be done on metabolites before reactions
	if ("cofactor" in n.raw) {
		n.cofactor=(n.raw.cofactor?true:false)
	} else {
	    if ( metabolite(n) ) {         
			n.cofactor= false;		 
	    } else {
	        n.cofactor=true; 
			findNodes(n.adjacencyList).forEach( function(m,i) {
				n.cofactor = n.cofactor && m.cofactor;
			})    
	    }
	}	
}

 function defNodeColor(n) {
 	if ("color" in n.raw) {
		n.color = n.raw.color
	} else {
	    if (metabolite(n)) {
		    if (n.cofactor) 
  			    n.color = "#DDD";
		    else {
			    if (n.blacknode) 
				    n.color= "#E55";
			    else   
				    n.color= "#FBB";   //default metabolite color
			}
		}
		 else {
			n.color="#222" //default reaction color; changed by subsystems if any
		}
	}
 }
 /*function defElementSystem (n) {
	if ("subsystemID" in n.raw) {
		n.subsystemID=n.raw.subsystemID;
	}
	if ("subsystem" in n.raw) {
		n.subsystem=n.raw.subsystem;
	} 
 }*/
 
function defValue(n) {
	//must be done on reactions before metabolites
	if ("value" in n.raw) {
		n.value=+n.raw.value;
	} else {
	    if (metabolite(n)) {
		    var threshold=0;            
			findNodes(n.adjacencyList).forEach(function(r) {
				threshold=Math.max(threshold, Math.abs(r.value));
			});
			n.value=threshold;
	    } else {
		    n.value=1;
	    }
	}
}

 
function defCompartment(n) {
    //must be done on metabolites before reactions
	key="";
	if ("compartment" in n.raw) {
		key= n.raw.compartment
	} else {		
	    if (metabolite(n) &&        
			"id" in n.raw && 
			parameters.useLastIdLetterAsCompartment) {
//TODO				
			 if ((/_e[^_]*$/g).test(n.raw.id)) key= "e"
			 if ((/_c[^_]*$/g).test(n.raw.id)) key= "c"
			 if ((/_p[^_]*$/g).test(n.raw.id)) key= "p"   		 	
		
	   } else {
			var first;
			findNodes(n.adjacencyList).forEach( function(m,i) {
				if (i==0) 
				   first=m.compartment;
				else if (first!=m.compartment)
				   first=0;
			 })    
			 if (first) n.compartment = first;
			 return;
	   }
	}
	if (key != "") {
		checkComparmentExists(key);
		n.compartment = compartmentKeyMap[key];		
	}	
}

function defXY(n) {
	if ("x" in n.raw && "y" in n.raw) {
		n.x=n.raw.x;
		n.y=n.raw.y;
	} else {
		n.x=0;
		n.y=0;
	}
}
function defSplit(n) {
	if ("split" in n.raw) {
		n.split=[n.raw.split[0], idsToNodes(n.raw.split[1])];
	}
}
function  defOrientation(n) {
	if ("orientation" in n.raw) {
	   if (n.raw.orientation== "<" ||n.raw.orientation.toLowerCase() == "reverse") {
			n.orientation="<";
		} else if (n.raw.orientation== ">" ||n.raw.orientation.toLowerCase() == "direct") {
			n.orientation=">";
		} else {		
			n.orientation="#";			
		}
	} else {
		n.orientation = ">"; 
	}
}

function defExtendedAdjacencyList (n) {
	var l=n.adjacencyList;
    n.inAdjacencyList.forEach(function (x) {
        l=l.concat(data.nodes[x].inAdjacencyList)
    })
    n.outAdjacencyList.forEach(function (x) {
        l=l.concat(data.nodes[x].outAdjacencyList)
    })
    n.extendedAdjacencyList = l.filter(function(x) {return (!data.nodes[x].cofactor)}); 	  	
}

function defInOutStrongDegree(n) {   
}
function firstReadNodes(){
	nNodes = data.nodes.length;       
   
	data.nodes.forEach(function(n,i) {
		n.inFilter=true;   
		n.outsider=0;
		n.nearOutsider=0;  
		
      n.strongInDegree=0;
      n.strongOutDegree=0;
		n.nextStep = [];
		n.selected = false;
		defGroup(n); 
      defSplit(n);
		defSearchFields(n)
		defShortName(n); 
		defName(n); 
		defBlackNode(n);
	//	defElementSystem(n);
		if (reaction(n)) {      
			defValue(n)  
			defOrientation(n);     
			n.axis= {x:0,y:1};       
		} else {
			defCofactor(n);          
		}
	}) 
	data.nodes.forEach(function(n,i) {
		if (reaction(n)) {   
			//need to be defined on metabolites first   
			defCofactor(n);     
			defInOutStrongDegree(n);     
		} else {
			//need to be defined on reactions first
			defValue(n);
		}
		//need cofactor
		defNodeColor(n);
		defExtendedAdjacencyList(n);
	})  
   
}


function updateGravity() {
   data.nodes.forEach(function(d) {   
      d.gravityWell={
         x: CC.center[d.connectedcomponent].x,
         y: CC.center[d.connectedcomponent].y}
      if (parameters.compartmentPlacement && ("compartment" in d)){ 
         d.gravityWell.x += d.compartment.center.x;
         d.gravityWell.y += d.compartment.center.y;
      } 
      d.gravityStrength =1;
      if (d.outsider) d.gravityStrength = 1;
      else if (d.nearoutsider) d.gravityStrength = 1;
      d.gravityStrength *= parameters.coeffGravity;
   });
}






function writeColor(d) {
   if (parameters.compartmentColor && metabolite(d)) {
		if (!("compartment" in d)) 
			return "black";		
      if (d.blacknode)
         return d.compartment.darkColor;
      return d.compartment.color;
   } else {
		return d.color;
   }         
 }
 
 function writeNodeStrokeWidth(d) {
	 return "1.5px";
 }
function writeNodeCharge(n) {   
   return -1*(n.degree * 300 +3000)*parameters.coeffCharge;
}

function writeReactionPath(n) {
   if (n.orientation == "#")  
      return "M-22 0 L-14 9 L14 9 L22 0 L14 -9 L-14 -9 Z";
   else if (n.orientation == "<") 
      return "M 10 0 L18 9 L-12 9 L-20 0 L-12 -9 L18 -9 Z";
   else 
      return "M-10 0 L-18 9 L12 9 L20 0 L12 -9 L-18 -9 Z";
}

function writeReactionLabel(n) {
   if (!("value" in n.raw) || n.raw.value==0) return "";
   if (n.raw.value>0) {
      if (n.orientation == "#")
         return "&#8722;+";
      else 
         return "+";
   } else {
      if (n.orientation == "#")
         return "+&#8722;"; 
      else 
         return "&#8722;";
   }
}

 
function writeClass(d) {
	return (d.group==1?"reaction":"metabolite") + 
		(d.cofactor ? " cof" : " notcof") +  
		(d.blacknode ? " bn" : " wn") + 
		" node CC"+d.connectedcomponent;
}
function trimIdLeftAndRight(t) {
	t=t.substring (parameters.trimIdLeft) ;	
	return t.substring (0, t.length-parameters.trimIdRight)	
}
 function writeMetaboliteLabel(d){
	 if (parameters.autoLabel ==1) {
			
		if (!"name" in d.raw || d.raw.name.length > 9) {
		  var show = d.shortName
		  show=show.substring (parameters.trimIdLeft) 
		  show=show.substring (0, show.length-parameters.trimIdRight)
		  d.metaboliteLabel = [[show,"italic"]];
		} else {
		  d.metaboliteLabel = [[d.raw.name,"normal"]];
		}   
	 } else {
		 d.metaboliteLabel=[];
		 if (parameters.showLabelName) d.metaboliteLabel.push([d.name,"normal"]);
		 if (parameters.showLabelId) d.metaboliteLabel.push([trimIdLeftAndRight(d.raw.id),"italic"]);
		 if (parameters.showLabelShortName) d.metaboliteLabel.push([trimIdLeftAndRight(d.shortName),"italic"]);
		 if (parameters.showLabelCompartment && ("compartment" in d)) d.metaboliteLabel.push([d.compartment.name,"normal"]);		 
	 }
}

function updateNodeLabels() {
	   var tspans=
 		metabolitenode
		 .each(writeMetaboliteLabel)
       .select("text")            
       .style("font-size", writeFontSize)		 	 
       .selectAll("tspan")
		 .data(function(d){return d.metaboliteLabel});
		tspans.exit().remove();
		tspans.enter().append("tspan");
		tspans		 
       .attr("font-style",function(d){return d[1];})  
       .attr("dy","1.1em")
       .attr("x",0)                    
       .text(function(d){return d[0];}) ;
		metabolitenode
		 .select("text")
		 .attr("y", function() {
			return (-1*this.getBBox().height/2)+"px";
		 })
}

function writeFontSize(d) {
	if (d.metaboliteLabel.length)
		return  (Math.floor(14*parameters.nodeScale)*Math.max(1, (d.cofactor?1.5 : 2.5)/Math.sqrt(d.metaboliteLabel[0][0].length+1)))+"px"
	return "10px";
}
function setNodeList(datanode) {
    var x=d3.select("#nodecontainer").selectAll(".node")
        .data(datanode, function(d) { return d.mainIndex;});
    
	 
   // exit: remove 
    x.exit().remove(); 
    
   // enter: <g> elements  
   
    var gs= x.enter().append( "g")
        .attr("id", function(d) {return d.id; })
        .attr("class", writeClass )
        .attr("transform", function(d) { return "translate("+d.x+","+d.y+")"; })
        .call(drag)
        ;
        
    
  
   // nodes : metabolites   
    metabolitesForward();    
       
    var mn=gs.filter(metabolite);   
    mn
        .on("mouseover", function(d) {
				setForward(d.id, ".node"); 
				hoverOn(d, "Metabolite") 
			})
        .on("mouseout", function(d) { hoverOff()})
        .on("click", function(d) { hoverClick(d, "Metabolite")})
        .append('circle')
        .attr("fill", writeColor)
        .attr('r', 35*parameters.nodeScale)        
        .attr("stroke", "#000")
        .attr("stroke-width", writeNodeStrokeWidth)
        .attr("opacity", "0.75")
        .attr('clip-path', function(d) { return 'url(#clip-'+d.id+')'; })
		  ;
    mn
       .append("text")
       .attr("x", 0)
       .attr("y", 0)
       .attr("text-anchor","middle")       
      // .attr("dominant-baseline","middle")
       .attr("fill","black")          
       .attr("stroke","black")        
       .attr("stroke-width",0.5)      
       .attr("font-family","sans-serif")        
       .style("cursor","default")      
	
	   
 
   
   
   //
   // nodes : reactions
   //
    var rn=gs.filter(reaction)
       .attr('clip-path', function(d) { return 'url(#clip-'+d.id+')'; })
       .append("g") 
       .attr("class","rotateBox");
         
    rn
         .on("mouseover", function(d) {  hoverOn(d, "Reaction") })
         .on("mouseout", function(d) { hoverOff()})
         .on("click", function(d) { hoverClick(d, "Reaction")})
         .append('circle') 
         .attr("class", "highlightBG")
         .attr('r', 35*parameters.nodeScale*1.5)
         .attr("cx",0)
         .attr("cy",0)
         .attr("fill",writeLinkColor)
         .attr("opacity", 0);
    rn
         .append("g")
         .attr("transform", "scale("+parameters.nodeScale+")")         
         .append("path")
         .attr("d", writeReactionPath )
         .attr("fill", writeLinkColor)
         .attr("stroke", "#fff")
         .attr("stroke-width", "1.5px")
         ;
    rn
         .append("text")
         .attr("y", 0)// 7 * parameters.nodeScale)
         .attr("x", 0)
         .style("font-size", (Math.floor(23*parameters.nodeScale))+"px")
         .attr("text-anchor","middle")     
         .attr("dominant-baseline","middle")  
         .attr("fill","white")          
         .attr("stroke","white")       
         .style("color", "white")
         .style("cursor","default")  
         .html(writeReactionLabel);
        
    node = container.selectAll(".node");
    metabolitenode = node.filter(metabolite);  
    reactionnode=node.filter(reaction);    
     
	updateNodeLabels();
     
    force.nodes(datanode); 
    
    log("Nodes ok (showing "+ datanode.length+ ")",1)

}

function updateNodeView(){
   //call for new nodeScale or new reaction label
   
   var m= d3.selectAll(".metabolite");
   m.select("circle")      
      .attr('r', 35*parameters.nodeScale)
   m.select("text")   
       .style("font-size", writeFontSize)
   var rn=d3.selectAll(".reaction").select("g");
       
         
    rn
         .select('circle') 
         .attr('r', 35*parameters.nodeScale*1.5)
    rn
         .select("g")
         .attr("transform", "scale("+parameters.nodeScale+")")        
         ;
    rn
         .select("text")
         .attr("y",0)// 7 * parameters.nodeScale)
         .style("font-size", (Math.floor(23*parameters.nodeScale))+"px")
         .html(writeReactionLabel);

   svg.selectAll(".selnode")
      .select("circle")
      .attr("r",35*parameters.nodeScale*2)
      
	updateNodeLabels();
   updateView();
         
}
function updateDynamicsCoeff() {
	updateGravity();
	if (force)  force.charge(writeNodeCharge);
   
	updateForceStatus();
	
}

//!
//! split nodes
//!

//-> create d copies of a given node, where d is the degree.
//- the original node remains in data.nodes
// with a "split" field containing ["original", [array of copies]]
//- copies are placed at empty slots in data.nodes
// with a "split" field containing ["copy", orignal node]
//- joining deletes the copies and the split field of the original


function replaceInLink(l, before, after) {

   if (l.source==before) l.source=after;            
   if (l.target==before) l.target=after;
   if (l.metabolite==before) l.metabolite=after;  
   if (l.reaction==before) l.reaction=after;  
   
}
function newIndex() {
   for (var i=0; typeof data.nodes[i]!="undefined"; i++);
   return i;   
}

function splitNode(n) {
   if (n.split || n.adjacencyList.length<2 ) return;
   
   log("split "+n.name,8)
   var newNodes=[];
   var sel=n.selected;
   selectionRemove([n]);
   n.adjacentLinks.forEach(function(l) {
      var nn= shallowCopy(n);
      nn.id=nn.mainIndex=newIndex();
      data.nodes[nn.id]=nn;
      
      var otherEnd=(l.target==n ? l.source : l.target);
      
      log(l,8);
      log(n.x,8);
      log(otherEnd.x,8);
      
      
      replaceInLink(l, n, nn);      
      
      if (typeof n.x!= "undefined") {
         nn.px=nn.x=0.5*nn.x+0.5*otherEnd.x;
         nn.py=nn.y=0.5*nn.y+0.5*otherEnd.y;               
         
         log(nn.x, 8)
      } else {
           log(typeof n.x, 8)
        
      }
      nn.split=["copy", n];
      newNodes.push(nn);
   })
   n.split=["original", newNodes];
   log([n,newNodes],8)
   selectionAdd(newNodes); 
}

function joinNode(n) {
   if (!n.split) return;
   if (n.split[0]=="copy") {
      n = n.split[1];
   }
   copies=n.split[1];
   delete n.split;
   
   selectionRemove(copies);
   
   copies.forEach(function (nn) {
      nn.split[0]="deleted";
      nn.adjacentLinks.forEach(function(l) {         
         replaceInLink(l, nn, n);             
      })      
      delete data.nodes[nn.id];
   })
   c=quickCenter(copies, n);
   n.x=c.x;
   n.y=c.y;
   selectionAdd([n]); 
}


function clanUpAfterSplitOrJoin() {
   
   neighborhood();
   updateSystemNodes ();
   filterOut();
   fixToolTips(false);
   if (data.rigidGroups) {
      data.rigidGroups.forEach(function(g,i) {
         g.nodes = g.nodes.filter(function (n) {
            if (n.split && (n.split[0]=="deleted" || n.split[0] == "original")) {
               delete n.rigidGroup;
               return false;
            }         
            return true;
         })
         recenterGroup(g) 
      })    
      killTrivialGroups();
      updateRigidGroupsList();      
   }
   
}
function splitAndClean(index) {
   splitNode(data.nodes[index]);
   clanUpAfterSplitOrJoin();
}
function joinAndClean(index) {
   joinNode(data.nodes[index]);
   clanUpAfterSplitOrJoin();
}







//!
//!   Link functions - brother links
//! 


function addCliqueLinks(ids) {    
   ids.forEach(function(source) {
      ids.forEach(function(target) {
         if (source<target) {
            data.extraLinks.push({
               source:source,
               target:target,
               original:false,
               cliqueSize:ids.length,
               value:Math.max(Math.abs(source.value), Math.abs(target.value))  
                                        
            });
         }
      });
   });

}

function addBrotherLinks(){
   data.extraLinks = []
   data.nodes.forEach(function(r) {
      if (reaction(r)) {
         addCliqueLinks(r.inAdjacencyList);
         addCliqueLinks(r.outAdjacencyList);
      }
   })
}

//!
//!    Link functions -- first classification
//!




function firstReadArcs(){
   data.links.forEach(function(l) {
      l.original = true;
      l.outward = reaction(l.source);
      if (l.outward)  {
         l.metabolite = l.target
         l.reaction = l.source
      } else {
         l.metabolite = l.source
         l.reaction = l.target     
      }  
	  l.color="#222";
	  l.value=l.reaction.value;
	  l.name=l.reaction.name;
	  l.cofactor=(l.source.cofactor || l.target.cofactor);	  
	  l.id=l.source.id+"--"+l.target.id;
	 // defElementSystem(l);
	  l.source.nextStep.push(l.target.mainIndex);
	  if (l.reaction.orientation == "#") {
		  l.target.nextStep.push(l.source.mainIndex);
	  }     
	  if (!l.cofactor) {
	      l.reaction["strong"+(l.outward?"Out":"In")+"Degree"] += 1;
	  } 
   })
}


function computeMedianValue() {
	var values = data.nodes
		.filter( reaction )
		.map( function (r){return  (Math.abs(r.value))} )
		.sort( function(a,b) {return a - b}) ;
		
    medianReactionValue =  values[ Math.floor(values.length /2 ) ];
	
}


//!
//!    Link functions -- display
//!



function writeArcStrength(d) {
   return (d.original? 1: 0.1)*0.8; //TODO
}
function defArcDistance(d) {
   d.arcDistance = (d.original? 40 : 2* Math.pow(d.cliqueSize,0.5))*parameters.coeffArcDistance;
}
//   return (d.original? 25 * Math.pow(d.metabolite.degree,0.5) : 2* Math.pow(d.cliqueSize,0.5))*parameters.coeffArcDistance;
function writeArcDistance(d) {
   if (!d.arcDistance) {
      defArcDistance(d);
   }
   return d.arcDistance;
}

function updateArcParameters() {
   data.links.forEach(defArcDistance);
   force.linkDistance(writeArcDistance);
   updateForceStatus();
   
}

function writeArcClass(d) { 
	return "link";
}

function writeStrokeWidth(d) { 
   d.strokewidth= 
    Math.exp(
	Math.min(Math.log(parameters.arcMaxThickness),  
       (Math.abs(d.value))
	  * (Math.log(parameters.arcUnitThickness-parameters.arcMinThickness))/medianReactionValue
	  + Math.log(parameters.arcMinThickness)	 
	 ) 
	 )

   return d.strokewidth; 
}


function writeArcPath(l) {
   var path= "M "+l.source.x+","+l.source.y ;
   var sign=l.outward ? +1 : -1;
   var d1=sign*15*parameters.nodeScale,
       d2=d1+sign*50*parameters.arcCurveScale;
   
    if (l.outward) {
      path+=" L "+(l.reaction.x +d1*l.reaction.axis.showX) + "," + (l.reaction.y +d1*l.reaction.axis.showY);
      path+=" Q "+(l.reaction.x +d2*l.reaction.axis.showX) + "," + (l.reaction.y +d2*l.reaction.axis.showY);
   } else {
//      path+=(l.reaction.x -l.reaction.axis.showX*80*parameters.arcCurveScale) + "," + (l.reaction.y -l.reaction.axis.showY*80*parameters.arcCurveScale);
      path+=" Q "+(l.reaction.x +d2*l.reaction.axis.showX) + "," + (l.reaction.y +d2*l.reaction.axis.showY);
      path+=" "+(l.reaction.x +d1*l.reaction.axis.showX) + "," + (l.reaction.y +d1*l.reaction.axis.showY) + " L";
     
   }
   path+=" "+l.target.x+","+l.target.y
   return path;
}


function writeLinkColor(d) {
	return d.color;   
}

function setLinkList(datalink, extra) {
   //insert links
   
   nested_links = d3.nest()
		.key(function(d) { return d.reaction.id; })
		.entries(datalink);
	
	
   var boxes=d3.select("#linkcontainer").selectAll(".linkBox").data(nested_links)
   
   boxes.exit().remove();
   boxes.enter().append("g")
      .attr("class","linkBox")
	  .style("filter", "url(#shadow)")
   boxes.attr("id",function(d) { return boxName(d.key)});
   
   var x= boxes.selectAll(".link").data(function (d){return d.values});
   
   x.exit()
        .remove();
   x.enter()
        .append("path")   
        .attr("fill", "none")
        .attr("stroke-opacity", "1")
        .attr("stroke-linecap", "round")

	x.attr("class",  writeArcClass)
         .on("mouseover", function(d) {  hoverOn(d.reaction, "Reaction") })
         .on("mouseout", function(d) { hoverOff()})    
        //.attr("d" , writeArcPath)
        .attr("stroke", writeLinkColor)
        .style("stroke-width",writeStrokeWidth )  ;          
    
    link = container.selectAll(".link");
   
   force.links(datalink);//.concat(extra));
    
    
    log("Links ok (showing "+ datalink.length+ ")",1)
        
}



//!
//! Curves for hyperedges
//!

function lineDistance( point1, point2 ){
    var xs = 0;
    var ys = 0;     
    xs = point2.x - point1.x;
    xs = xs * xs;     
    ys = point2.y - point1.y;
    ys = ys * ys;     
    return Math.sqrt( xs + ys );
}

function lineAxis( point1, point2 )
{
    var xs = 0;
    var ys = 0;     
    xs = point2.x - point1.x;     
    ys = point2.y - point1.y;   
    l=Math.sqrt( xs*xs + ys*ys );    
    if (l==0) return {x:0, y:1};         
    return {
      x: xs/l, 
      y: ys/l 
     };
     //{x: - ys/l, y: xs/l };
}

function quickCenter(nodes,def) {
   var x=0;
   var y=0;
   var i=0;
   nodes.forEach(function (n) {
      if (n.inFilter) {
         x+=n.x;
         y+=n.y;
         i++;
      }
   });
   return i ? {x:  x/i , y: y/i} : (def?def:{x:0,y:0});
}

function center(ids, def) {
   var x=0;
   var y=0;
   var nFound=0;
   findNodes(ids).forEach(function (n) {
      if (n.inFilter) {
         var weight=(n.cofactor?1:10)
         nFound+=weight;
         x+=n.x*weight;
         y+=n.y*weight;
      }
   });
   if (!nFound) return {x:def.x, y:def.y};
   return {x: (x/nFound), y: (y/nFound)}
}

function updateAxis(noTopDown) {
	log("update axis", 2)
   var stepAngle=(parameters.strongOrientationAngle? Math.PI/parameters.strongOrientationAngle:0.001)
   data.nodes.forEach(function (r) {
      if (reaction(r)) {
         var inCenter = center(r.inAdjacencyList, r);
         var outCenter = center(r.outAdjacencyList, r);
         r.axis = lineAxis(inCenter, outCenter);  
         
       //  if (stepAngle>0.001|| parameters.orientTopDown) {
            var swapSign=1, orientingCoeff=1;
            if (parameters.orientTopDown && ! noTopDown) {
               var orientingCoeff=1-parameters.orientTopDown;
               if (r.orientation=="<") swapSign*=-1;
               if (r.value<0  && parameters.oppositeSignOppositeDirection) swapSign*=-1;
            }
            var angle=Math.acos(r.axis.y*swapSign)*(r.axis.x<0? -1:1) ; 
               //! angle measured vs. vertical axis
               //! in [-Pi ,+Pi] range 
               //! vertical axis flipped according to swapSign
               
            var targetAngle = angle;
            if (stepAngle>0.001) {
               targetAngle = (Math.round(angle*orientingCoeff/stepAngle))*stepAngle;
            } 
                
            if (r.axis.targetAngle != targetAngle) {    
               r.axis.targetAngle=targetAngle;
               r.axis.x=Math.sin(targetAngle);               
               r.axis.y=swapSign * Math.cos(targetAngle);
               
            }
         /*   var thisAngle;
            if (stepAngle>0.15) {
               thisAngle = (Math.round(angle/stepAngle))*stepAngle;
            }else {
               thisAngle=targetAngle;
            }*/
            r.axis.showX=r.axis.x;               
            r.axis.showY=r.axis.y;            
         //} 
           
      }
   });
   
}






   /*************** PART 2  *************/
   /*   Systems                         */
   /*   Focus/Highlight                 */
   /*   Compartments                    */
   /*   Filter                          */
   /*   Menu                            */
   /*   Tooltip Box                     */
   /*                                   */
   /*************************************/   
   
   
   
//!
//! systems
//!



function checkSystemOpacity() {
	d3.selectAll(".showSystem div")
	   .each(function(d) {
		//style("opacity",	
		    for (var i=0; i<d.systemNodes.length; i++) {
				var n=data.nodes[d.systemNodes[i]]
				if (reaction(n) && n.inFilter) {
				   d.inFilter=true;
					return;// "1";
				}
			}
			d.inFilter=false;
			//return "0.4"
		})
		.style("opacity",function(d) {return d.inFilter?"1":"0.4"});
		 
	
}

function importOlderVersions() {
	//systems organized as associative map => array
	if (typeof data.systems != 'undefined' && !("length" in data.systems)) {
		var ds=data.systems;
		data.systems = Object
			.keys(ds)
			.map(function(s){
				var r = ds[s];
				r.id=s;
				return r;
			})
		log(data.systems,3);
	}	
}
		

function updateSystemNodes () {
   data.systems.forEach( function (s) {		
      s.systemNodes=[];
   });   
   data.nodes.forEach( function (n, i) {	
      var s=n.system;	
      if (s) {
         s.systemNodes.push(n.mainIndex);
         s.systemNodes=s.systemNodes.concat(n.adjacencyList);  
      }
   }); 
   data.systems.forEach(function(s){
	    s.systemNodes=uniq(s.systemNodes);
	});
}
function matchSystemsToNodes () {
	data.nodes.forEach( function (n, i) {
		var key="";
		if ("subsystemID" in n.raw) key=n.raw.subsystemID;
		else if ("subsystem" in n.raw) key=n.raw.subsystem;
		
		if (key!="") {
			
			//log(key,3);
         s=systemKeyMap[key];
			//log(s,3);
			n.system=s;                    
         s.value=Math.max(s.value, Math.abs(n.value));        
			n.color = s.color;
         } 
    });
	data.links.forEach( function (l) {
		var key="";
		if ("subsystemID" in l.raw) key=l.raw.subsystemID;
		else if ("subsystem" in l.raw) key=l.raw.subsystem;
		
		if (key!="") {
            s=systemKeyMap[key];
			l.system = s;                       
            l.color = s.color;
         } 		
	});   
   updateSystemNodes () ;   
	
}
	
	
var systemKeyMap = {};

function firstReadSystems() {	
	
	var color = d3.scale.category20();
	if (!data.systems) {
		data.systems=[];
	}	
	data.systems.forEach(function(s,i) {
		s.id=i;
		var key=i;
		if ("id" in s.raw)
			key=s.raw.id;
		else if ("name" in s.raw)
			key=s.raw.name;
		
		systemKeyMap[key]=s;		
		
		
		if (!("name" in s)){
			if ("id" in s.raw)
				s.name=s.raw.id;
			else				
				s.name="System "+(i);
		}
		if (!("color" in s)) {
			s.color=color(s.name);
		}
		if (!("showSubSystem" in s)) {
			s.showSubSystem=true;
		}		
		s.systemNodes=[];
		s.value=0;
	});
	 matchSystemsToNodes();
    log("Systems ok ("+ data.systems.length+ " systems)",1)
}


function checkAll() {
    d3.selectAll('input').property('checked', true);
}
function uncheckAll() {
    d3.selectAll('input').property('checked', false);
}

function   fillSystemBox() {

	viewSubsystems=d3.select("#viewSubsystems");
	
	if (data.systems) {	
	   systemBox = viewSubsystems.selectAll(".showSystem")
		   .data(data.systems)
		   .enter()
		   .append("div") 
		   .attr("class","showSystem")
		   .on("mouseover", function(d) { if (d.inFilter){ hoverOn(d, "Subsystem") }})
           .on("mouseout", function(d) { hoverOff()})


		systemBox.append("div")   
		   .attr("id",function(d){return "system_"+d.id})
		   .style("background-color",function(d){return d.color})
		   .text(function(d){return d.name})
		   .on("click", 
				function(d){//highlight(d.id)
					if (d.inFilter) {
						hoverClick(d, "Subsystem");
						zoomOnNodes(d.systemNodes);
					}
				}
				);
		systemBox.append("input")
			.attr("class","selectSubsystem")
			.attr("id",function(d) {return "selectSS-"+d.id})
			.attr("type","checkbox")
			.property("checked",true)
			.on("change",function() {updateFilterSubSystems()});
		      
      viewSubsystems.append("span")
			.attr("class","ssButton")
			.on("click",function() {d3.selectAll(".selectSubsystem").property("checked", true); updateFilterSubSystems()})
			.text("Select all")
      viewSubsystems.append("span")
			.attr("class","ssButton")
			.on("click",function() {d3.selectAll(".selectSubsystem").property("checked", false); updateFilterSubSystems()})
			.text("Unselect all")
			
    /*  viewSubsystems.append("div")
         .attr("id","clearHighlight")
		   //.attr("class","showSystem")
		   .style("background-color","#bbb")
		   .html("<i>Clear highlight</i>")
		   .style("display", "none")
		   .style("color", "black")
		   .style("cursor", "pointer")
		   .style("text-align", "center")
		   .on("click",clearHighlight);*/
    } else {      
      viewSubsystems.append("div")
         .html("<i>No pathway information available</i>")
		   .style("color", "black")
		   .style("background-color","#bbb")
		   .style("text-align", "center");
		systemBox=d3.select('empty')   		   
    }
		
}

//!
//!        Focus & highlight
//!



function highlight(selector) {

	d3.selectAll(".highlight").classed("highlight", false);
	if (selector != "") d3.selectAll(selector).classed("highlight", true);
}
function zoomOnNodes(nodeIds) {
	
	if (nodeIds!=[]) {
		log("Zoom on "+(nodeIds?nodeIds.length: "all")+" nodes",2);
	   var xMin=Infinity;
      var xMax=-Infinity;
      var yMin=Infinity;
      var yMax=-Infinity;
      var keep=0;
      var test=function(n) { 
         if (n.inFilter) {
           keep=1;
           xMin=Math.min(xMin, n.x);    
           xMax=Math.max(xMax, n.x);    
           yMin=Math.min(yMin, n.y);    
           yMax=Math.max(yMax, n.y);         
        }
      }
      if (typeof nodeIds == "undefined"){
         data.nodes.forEach(test);
      } else {
         nodeIds.forEach(function (id) {
           test(data.nodes[id])       
         });
      }      
      if (keep) {
         xMin-=40*parameters.nodeScale;
         xMax+=40*parameters.nodeScale;
         yMin-=40*parameters.nodeScale;
         yMax+=40*parameters.nodeScale;
         var w=xMax-xMin;      
         var h=yMax-yMin;
         
        /* container.append("rect")
	            .attr("x",xMin-w/2)
	            .attr("y",yMin-h/2)
	            .attr("width", 2*w)	         
	            .attr("height", 2*h)
	            .style("opacity",0.2);*/
	      var border=0.1;      
	      var rx=(1+2*border)*w/width,
	          ry=(1+2*border)*h/height,
	          r=Math.max(Math.max(rx,ry),1),
	          tx=width/2 - (xMin+w/2)/r,
	          ty=height/2 - (yMin+h/2)/r;
	          
	          
	      zoom.scale(1);    
	      zoom.translate([0,0]); 
	      d3.select(".drawarea").attr("transform","translate(0,0)scale(1)");  
	      container.attr("transform", "translate("+tx+","+ty+")scale("+(1/r)+")");   
			
       }
	}
	trackZoom = nodeIds;
	log("Zoom done",2);
}

function highlightAndZoom(nodeIds) {
    log("highlightAndZoom : "+nodeIds,2);
	d3.selectAll(".highlight").classed("highlight", false);
	
	hoverStyle([]);
	
	node.classed("highlight",function (d,i) {	  
	   return (nodeIds.indexOf(d.mainIndex)>-1);
	});
	
	
    zoomOnNodes(nodeIds);
	//d3.select("#clearHighlight").style("display", "block");
}

function clearHighlight() {
   d3.selectAll(".highlight").classed("highlight", false);
	hoverStyle([]);
	//d3.select("#clearHighlight").style("display", "none");
}


function setForward(id, selector) {
	svg.selectAll(selector).sort(function (a, b) { // select the parent and sort the nodes
		if (a.id != id) return -1;                  // a is not the selected element, send "a" to the back
		else return 1;                              // a is the selected element, bring "a" to the front
	});
}

function quickForward(node) {
	node.parentNode.appendChild(node);
}

function metabolitesForward() {
	svg.selectAll(".node").sort(function (a, b) { 
   	lasttruc = a;
		if (!metabolite(a)) return -1;   
   	else return 1;
	});
}




//!
//!   compartment separation
//!


var compartmentKeyMap= {};
   
	
function fillCompartmentCentersAndColors () {
	var nc= data.compartments.length;
	
	var color = d3.scale.category20();
	data.compartments.forEach(function(c, i) {
		if (!("center" in c)) 
			c.center={x: Math.cos(i/nc*6.283)*300, y:Math.sin(i/nc*6.283)*300};
		if (!("color" in c) )
			c.color=color(c.name);		
		if (!("darkColor" in c) )
			c.darkColor=color(c.name);		
	})
}
function checkComparmentExists(c) {
	if (! (c in compartmentKeyMap)) {
		var s={"name":c, "id":data.compartments.length};		
		data.compartments.push(s);		
		compartmentKeyMap[c] = s;
	}
}
function matchCompartmentsToNodes () {
	data.nodes.filter(metabolite).forEach(defCompartment);
	data.nodes.filter(reaction).forEach(defCompartment);
}

function firstReadCompartments() {	
	
	if (!data.compartments) {
		data.compartments=[];
	}		
	
	data.compartments.forEach(function(c,i) {
		c.id=i;
		var key=i;
		if ("id" in c.raw)
			key=c.raw.id;
		else if ("name" in c.raw)
			key=c.raw.name;
		
		compartmentKeyMap[key]=c;		
		
		
		if (!("name" in c)){
			if ("id" in c.raw)
				c.name=c.raw.id;
			else				
				c.name="Compartment "+(i);
		}
	});
	matchCompartmentsToNodes();
	fillCompartmentCentersAndColors();
   log("Compartments ok ("+ data.compartments.length+ " compartments)",1)
}

	
function updateCompartmentParameters() {   
   node.selectAll("circle").attr("fill", writeColor);
   updateGravity();       
   
}


//d3.select("#chooseCompartment").property("checked", parameters.compartmentPlacement);
//d3.select("#chooseColorCompartment").property("checked", parameters.compartmentColor);





//! Far gravity

function updateComfortDistance() {
   CC.filterSize= CC.filterSize.map(function(){return 0});
   data.nodes.forEach(function(n) {
      if (n.inFilter) CC.filterSize[n.connectedcomponent]++;
   });
   data.nodes.forEach(function(n) {
      // comfort:          sqrt(       number of nodes    *        "cushion area" around each node  *  pi   ) * node radius
      n.comfortDistanceSquare = CC.filterSize[n.connectedcomponent] * parameters.nodeComfortArea * 3.15*35*35 * parameters.nodeScale* parameters.nodeScale;
   });
}


//!
//!   Filtering nodes
//!

//  based on value

   
var thresholdArray=[];

function createThresholdArray() {
  thresholdArray= [];
  var values= [];
  data.nodes.forEach(function(n) {
         if (reaction(n)) values.push(Math.abs(n.value));
      });
  values.sort(function(a, b){return a-b});    
  for (var i=0; i<100; i++) {
    thresholdArray.push(values[Math.floor(values.length*i/100)]);
  }  
}

function updateFilter(skipFilterOutStep) {  
	log("updateFilter",4)
   if (!skipFilterOutStep) {
	   filterOut();
   }
}


// hiding cofactors

function updateShowCofactors(skipFilterOutStep) {
	
	log("updateShowCofactors",4)
    //parameters.showCofactors=d3.select("#showCofactors").node().checked;
    if (!skipFilterOutStep) { 
		filterOut();   
    }
}

// based on subsystems


function updateFilterSubSystems(skipFilterOutStep) {
	log("updateFilterSubSystems",1)
	var countHidden=0;
	data.systems.forEach(function(s) {
		s.showSubSystem = d3.select("#selectSS-"+s.id).property("checked");		
		if (!s.showSubSystem) countHidden+=1;
	})
	d3.select("#filterLabelSubsystem").text(countHidden?countHidden+" subsystem(s) hidden" : "");
	if (!skipFilterOutStep) { 
		filterOut();   
    }
}

function hasLinkInFilter(node) {
	var result= false;
	node.adjacentLinks.forEach(function(l) {result=result || l.inFilter})
	return result;
}

// main filter
function filterOut(skipConvergence) {  
  log("filterOut "+skipConvergence,2)
  
  var ratio = parameters.filterRatio;
  var cof = parameters.showCofactors;
  
  log("filterOut "+skipConvergence+ " ratio "+ ratio+ "  cof "+cof,4)
  if (thresholdArray.length==0) {
    createThresholdArray() 
  }
  var threshold = thresholdArray[Math.floor(ratio)];
  
  log(threshold,4)
  var linkList = data.links.filter(function(l) {
      l.inFilter= (Math.abs(l.value)>=threshold) && (cof || !l.cofactor) && (!l.system  || l.system.showSubSystem) && !l.source.hideSel && !l.target.hideSel;
      return l.inFilter;
  });
  var nodeList = data.nodes.filter(function(n) {
      n.wasInFilter= n.inFilter;
      n.inFilter=(!n.split || n.split[0]=="copy") && hasLinkInFilter(n);
	  //Math.abs(n.value)>=threshold && (cof || !n.cofactor) && (systemsArray[l.subsystemID].inFilter);
      return n.inFilter;
  });
  var extraList = []; /*data.extraLinks.filter(function(l) {
      l.inFilter=Math.abs(l.value)>=threshold && (cof ||  !l.source.cofactor || !l.target.cofactor)
      return l.inFilter;
  });*/
  
  
   newSelection(); 
  
  
  var exactRatio=Math.floor(nodeList.length/nNodes*100);
  d3.select("#filterLabel")
    .html("threshold: "+threshold.toPrecision(2)+"<br>(Showing "+exactRatio+"% of all nodes)" );
  
  setLinkList(linkList, extraList);
  setNodeList(nodeList);
  recenterRigidGroups();
  updateComfortDistance();
  if (!skipConvergence) {
	   convergeForce(1, "new"); 	  
  } else {
      updateView();  
  }
  checkSystemOpacity(); 
  zoomOnNodes(); //zoom fit the whole drawing 
  log("filtered out",1)
}

//!
//! Menu Bar
//!


buttons=["Fluidity","Compartments","Selection","Filter","Search", "Photo","Pathways"];
active={};
nActives=0;

	
function activate(b, status) {
	
     nActives=nActives + (status?1:0) - (active[b]?1:0);
     active[b] = status;   
     
        
     d3.select("#opt"+b).style("display",active[b]?"block":"none");
     d3.select("#"+b).classed("active", active[b]); //.style("background-color",active[b]?"#f8ffDf":"#EEE");     
     //d3.select("#closeOptions").style("display",nActives>0?"block":"none");
     d3.select("#options").style("display",nActives>0?"block":"none");
	 windowMarginHor=10 + (nActives>0 ? 400 : 0 );
}

buttons.forEach(function(b) {
  activate(b,false);
  d3.select("#"+b).on("click",function() {
    //activate(b, !active[b]);
   var ctrl= d3.event.ctrlKey;
	buttons.forEach(function(c) {
      
      activate(c, (b==c && !active[b]) || (b!=c && ctrl&& active[c]));
	   updateWindow();
    })   
  })    
})




d3.select("#closeOptions").on("click",function() {
   buttons.forEach(function(b) {
      activate(b,false);
   })   
  updateWindow();
})
		

		
//!
//! Search feature
//!		


// pressing enter in the text field triggers the searchElements function
d3.select("#searchText").on("keypress", function(e) {
   if (d3.event.keyCode == 13) {
   	searchElements(d3.select("#searchText").node().value, "searchBox");
   }
})
// the search Button triggers the searchElements function
d3.select("#searchButton").on("click", function() {
	searchElements(d3.select("#searchText").node().value);
})
d3.select("#pathSearchButton").on("click", searchPathways);

// get all 
function searchElements(what) {
	
	var results= [];
	
	//transform into capital text (= ignore case in search)
	what=what.toUpperCase();	
	
	// do the actual search: each node adds a corresponding "match" object to results if necessary
	data.nodes.forEach(findNode(what,results)) ;	
	
	// go for display
	displayResultsList(results,false);
	
	// show the "no Result" box if necessary
	updateNoResult(results.length==0, what); 
	
}

// get all 
function searchPathways() {
	
	var results= [];
    var c = chemins(startPath.mainIndex, endPath.mainIndex)
    results=c.map(function(d){
        return {
            object:d,
            value:-d.length,
            kind:"pathway",
            inFilter:true,
            highlightSel:d,
            selection:d
            }
    });
    console.log(results);
		
	
	// go for display
	displayResultsList(results,true);
	
	// show the "no Result" box if necessary
	updateNoResult(results.length==0, ""); 
	
}




// display a list of results in the #searchResults box
function displayResultsList(results,pathway) {

   //sort results by "value" field
	results.sort(sortResults);
	
	//assign an element of class "searchResult" within the "#searchResults" box for each result in the list
	var res = d3.select("#searchResults")
		.selectAll(".searchResult")		
		.data(results);
	
	//remove useless elements	
	res.exit().remove();
	
	//actualy create the new elements, give them mouse behaviours
	res.enter()
		.append("div") 
		.attr("class","searchResult")
		.classed("inFilter",get("inFilter"))
		.on("mouseover",hoverOnResult) 
		.on("mouseout",hoverOffResult)
		.on("click",clickResult);	
	
	//write down the results within in each box
    
    if (pathway){ 
        res.html(givePathway);
          
    }
    else {
        res.html(giveResult);
        d3.selectAll(".searchResult .start").data(results).on("click",setStart)
        d3.selectAll(".searchResult .end").data(results).on("click",setEnd)   
    }
}
var startPath
function setStart(resultnode){
    startPath=resultnode.object;
    d3.select("#pathSearchStart").text(startPath.name);  
    updateNoResult(false)     
}

function startPathFromToolTip() {
      
    startPath=currentShowingObject;
    d3.select("#pathSearchStart").text(startPath.name);
    activate("Search", true);
    updateNoResult(false)
}

var endPath
function setEnd(resultnode){
    endPath=resultnode.object;
    d3.select("#pathSearchEnd").text(endPath.name);
    updateNoResult(false)
}

function endPathFromToolTip() {      
    endPath=currentShowingObject;
    d3.select("#pathSearchEnd").text(endPath.name);
    activate("Search", true);
    updateNoResult(false)
}

//compute html text for a given result
function pathStartEndButtons() {
   return 'Path <button class="start"> Start </button><button class="end"> End </button>' ;
}
function giveResult(d) {
	var name='<span class="findName">'+d.object.name+'</span>';
	var id='<span class="findId">'+d.object.raw.id+'</span>';
	var extra='';
   var path='<span class="findPath">' + pathStartEndButtons()+ '</span>';
	   
	if (metabolite(d.object) && d.object.cofactor) {
		extra='<span class="findExtra">cofactor of '+data.nodes[d.object.adjacencyList[0]].name+'</span>'
	} else {
		extra='<span class="findExtra">'+d.object.group+'</span>'
	}
	
	return name+ path +'<br>'+ id+' ' + extra;
	
}

function givePathway(d,i){
	var name='<span class="findName">Pathway'+ i +'</span>';
	var id='<span class="findId">'+d.object+'</span>';
	var extra='<span class="findExtra">'+'Pathway length '+d.object.length+'</span>'+'<div style="clear:both"></div>';
   
  
	return name+'<br>'+ id+' ' + extra;
   
	
}
//mouse enters a result box
function hoverOnResult(d) {   
	hoverOn(d.object, d.kind, true, d.inFilter);	
}


//mouse leaves a result box
function hoverOffResult(d) {
	hoverOff();	
}
//select a result
function clickResult(d) {   
   if (d.inFilter) {
   	zoomOnNodes(d.highlightSel);	
	}
   
   toSel=d.selection;
   if (d3.event.ctrlKey) {				
      selectionToggle(toSel);				
   } else {	
      selectionDefine(toSel);
   }
   
	fixToolTips(true);
}
//sort results by value
function sortResults(a,b) {return b.value-a.value};

// toggle the "No results for ..." line
function updateNoResult(visible, what) {
	d3.select("#noResult").html(!visible ? "": "No results for <i><b>"+what+"</b></i>.");	
}


function findNode(what, results) {
   //create a result element if we have a match.
	return function(d) {
		var val=0;
			
		d.searchFields.forEach(function(f) {		
		
			if (f==what) {
				val+=10;
			} else {
				var i= f.indexOf(what);
				if( i==-1) return; //f doesn't match at all
				if (i==0) {
					val +=3; //f has a prefix
				} else {
					val ++; //f has "what" inside
				}
			}
			if (d.inFilter) {
				val+=5;
			}	
		});
		if (val == 0) return;//no match found here
		var highlightSel=[d.mainIndex].concat(d.adjacencyList);
		if (metabolite(d)) {
			highlightSel = highlightSel.concat(d.distance[2])
		}
		var hoverSel=[];
		//result element:
		results.push({
			value:val,           //integer representing the quality of the match (10 = exact name, 2=prefix, 1=any substring; +5 if visible)
			object:d,            //the actual object corresponding to the match
			kind:(metabolite(d)?"Metabolite":"Reaction"), //a string representing the kind of element we get. Used to define the behavior of "hoverOn"
			inFilter:(d.inFilter?true:false), //is there anything visible corresponding to this match?
			highlightSel:highlightSel,      //which elements should be highlighted on if the user clicks on this result 
         selection:[d]			
		})		
	}
	
}
//!
//! Snapshots
//!
		
 

d3.select("#save").on("mousedown", function(){
   var svgSrc = d3.select("svg")
   .attr("version", 1.1)
   .attr("xmlns", "http://www.w3.org/2000/svg")
   .node()
   .outerHTML
   .replace(/−/g, "-");
    
   //log(svgSrc);
   var href = 'data:image/svg+xml;base64,'+ btoa(svgSrc);
    d3.select("#save").attr("href", href);
//   window.open(imgsrc, '_blank');
//   var img = '<img src="'+imgsrc+'">';
//   d3.select("#svgdataurl").html(img);
 
}); 


d3.select("#saveJson").on("mousedown", function(){
   var jsonSrc = exportJson();
    
   //log(svgSrc);
   var href = 'data:application/json;base64,'+ btoa(jsonSrc);
    d3.select("#saveJson").attr("href", href);
//   window.open(imgsrc, '_blank');
//   var img = '<img src="'+imgsrc+'">';
//   d3.select("#svgdataurl").html(img);
 
}); 

//!		
//!  hovering tooltip info
//!

var currentToolTips = [];
var currentTitle = "";

function insertReactionsById(toolTips, ids) {
   toolTips.push(["Reactions"]);
   findReactions(ids).forEach(function(d) {
      toolTips.push([d.name, d.value]);   
   });

}
function insertNodes(toolTips, nodes) {
   var nR=nodes.filter(reaction);
   var nM=nodes.filter(metaboliteNotCof);
   var nC=nodes.filter(metaboliteCof);
   if (nR.length>0) {
      toolTips.push(["Reaction"+sLongList(nR)]); 
      nR.forEach(function(d) {
         toolTips.push(["",d.name]);   
      });
   }
   if (nM.length>0) {
      toolTips.push(["Metabolite"+sLongList(nM)]); 
      nM.forEach(function(d) {
         toolTips.push(["",d.name]);   
      });
   }
   if (nC.length>0) {
      toolTips.push(["Cofactor"+sLongList(nC)]); 
      nC.forEach(function(d) {
         toolTips.push(["",d.name]);   
      });
   }
}   

function hoverStyle(ids, subsystem) {
	node.classed("hoverStyle", function (d) {return ids.indexOf(d.mainIndex)>-1}); 
	var x=-1;
	if (subsystem) {
		x=subsystem.id;
	}
	systemBox.classed("hoverStyle", function (d,i) {return i == x});    
}

function highlightReaction(d) {
   var sel=d3.select("#"+boxName(d.id))
				.classed("highlightLink",true)
				.style("filter", "url(#shadowYellow)");
	if (!sel.empty()){
   	quickForward(sel.node())
	}
}
function highlightReactions( ids) {
   findReactions(ids).forEach(highlightReaction);
   

}

function boxName(name) {
	return "box-"+(name+"").replace(/\W/g,"x")
	
}
function pushToolTip(toolTips, caption, data) {
	if (typeof data!= "undefined") {
		toolTips.push([caption, data]);		
	}	
}

function pushRemainingToolTips(toolTips, d, exclude) {
   Object.keys(d).forEach(function(k) {
      if (exclude.indexOf(k)==-1) {         
         toolTips.push([capitalizeFirstLetter(k), d[k]]);	         
      }
   })
   
   
}
function pushSplitButton(toolTips, d) {
   var splitable=!d.split && d.degree>1;
   var splited=(d.split?d.split[1].split[1].length : false);   
   var group=typeof d.rigidGroup=="undefined" ? "": " The node is first deleted from its group.";
   var lhs = splited?("Split in "+splited+" nodes") : ("Degree "+d.degree);
   var rhs ='<button onclick="splitAndClean('+d.mainIndex+');" title="Split into one node for each adjacent arc.'+group+'" '+(splitable?'':'disabled')+'> Split</button>';
   rhs+= '<button onclick="joinAndClean('+d.mainIndex+');" title="Merge all nodes representing this object.'+group+'" '+(splited?'':'disabled')+'> Join</button>';
   pushToolTip(toolTips,lhs,rhs );
}
function pushPathButton(toolTips, d) {
   
   pushToolTip(toolTips,"Path search", 
      '<button onclick="startPathFromToolTip();" title="Look for paths starting on this node"> Start </button>'+
      '<button onclick="endPathFromToolTip();"  title="Look for paths ending on this node"> End </button>')
}

function hoverOn(d, kind, doToolTips,doHighlight){
    if (typeof doToolTips=="undefined") { doToolTips=true;}
    if (typeof doHighlight=="undefined") { doHighlight=true;}
	var toolTips=[];
	var title=kind;
	
	if (kind=="Reaction") {	   
		toolTips.push(["Name", "<b>"+d.name+"</b>"]);	 
		if (d.system) pushToolTip(toolTips,"Subsystem", d.system.name);
      pushRemainingToolTips(toolTips,d.raw, ["x","y","orientation","compartment","subsystem","subsystemID","name","id","group","shortname","shortName"]);
		if (d.raw.orientation) {
			toolTips.push(["Orientation",  d.orientation == ">" ? "Direct" :(d.orientation == "<" ? "Reverse": "Both ways")]);				
		}
		if (d.compartment)
			pushToolTip(toolTips,"Compartment", d.compartment.name);
		pushSplitButton(toolTips, d);
      pushPathButton(toolTips, d)
		findNodes(d.inAdjacencyList).forEach(function(n) {
			toolTips.push(["Substrate", n.name])
		});
		findNodes(d.outAdjacencyList).forEach(function(n) {
			toolTips.push(["Product", n.name])
		});
		if (d.system) hoverStyle(d.adjacencyList.concat(d.mainIndex), d.system)
		if (doHighlight) {
			highlightReaction(d);
			d.fixed=true;
	   }
		
	} else if (kind=="Metabolite") {	
		toolTips.push(["Name", "<b>"+d.name+"</b>"]);	 
		pushToolTip(toolTips,"Identifier", d.raw.id);	 
		if (d.blacknode) toolTips.push(["", "Measured node"]);	 
		if (d.cofactor) toolTips.push(["", "Cofactor"]);
      
		pushSplitButton(toolTips, d);  	
      pushPathButton(toolTips, d);
		if (d.compartment)
			pushToolTip(toolTips,"Compartment", d.compartment.name); 
      if (d.system) pushToolTip(toolTips,"System", d.system);	
      pushRemainingToolTips(toolTips,d.raw, ["x","y","cofactor","blacknode","compartment","subsystem","subsystemID","name","id","group","shortname","shortName"]);
		if (d.system) {
		   if (!d.cofactor) 
		     hoverStyle(d.extendedAdjacencyList.concat(d.mainIndex),d.system)
		   else 
		     hoverStyle(d.adjacencyList.concat(d.mainIndex),d.system)
      }
		if (doHighlight) {
		   highlightReactions(d.adjacencyList);
		   d.fixed=true;
		}
		
	} else if (kind=="Subsystem") {	
		title="Metabolic Pathway";
		toolTips.push(["Name", "<b>"+d.name+"</b>"]);	 
		hoverStyle(d.systemNodes);  
		if (doHighlight) {
   		highlightReactions(d.systemNodes);
		}
      pushRemainingToolTips(toolTips,d.raw, ["name","id","color"]);
		insertReactionsById(toolTips, d.systemNodes)	   	;
		
		
	} else if (kind=="Group") {	
		hoverStyle(d.nodes); 
      title="Group"; 
		/*if (doHighlight) {
			highlightReactions(d.systemNodes);
		}*/
		insertNodes(toolTips, d.nodes)	   	;
   } else if (kind=="pathway"){
        hoverStyle(d, "");  
		if (doHighlight) {
   		highlightReactions(d);
   	}
   }
   if (doToolTips){

		if (parameters.enableToolTipBox) {
	
		   d3.select("#viewDataDown")
			   .style("display","inline")
			   .style("border","2px solid yellow");
		   d3.select("#hoverData b").text(title);
		   d3.select("#hoverData table").selectAll("tr")
			   .data(toolTips).enter().append("tr").selectAll("td")
			   .data(function(d) { return d; }).enter().append("td")
			   .html(function(d,i) { return d + (i==0 && d!="" ? ":" : ""); });	
		}
   
		currentToolTips = toolTips;
		currentTitle = title;
      currentObject = d;
   }
}

function fixToolTips(onOff) {
	if (onOff) {
		
		d3.select("#viewFixedData")
			.style("display","block");
		d3.select("#viewFixedData b").text(currentTitle);
		var tr=d3.select("#viewFixedData table")
				.html("")
				.selectAll("tr")
				.data(currentToolTips);
		tr.enter().append("tr");
		tr.exit().remove();
		var td=tr.selectAll("td")
			 .data(function(d) { return d; }).enter().append("td")
		td.html(function(d,i) { return d + (i==0 && d!="" ? ":" : ""); });		
		d3.select("#options").attr("class", "reduced");
		currentShowingObject=currentObject
	} else {
	   d3.select("#viewFixedData").style("display","none");		
	   d3.select("#options").attr("class", "");
	}	
}

function closeFixedData() {
   
	nextClickIsADrag = false;
	nextClickIsAZoom = false;
	hoverClick();
}
function hoverOff(){
	hoverStyle([])
   
		  d3.selectAll(".highlightLink")
				.classed("highlightLink",false)
				.style("filter", "url(#shadow)")
		  
	updateFixed();

	var tr=d3.select("#hoverData table").selectAll("tr")
        .data([])
        .exit().remove();
         
	   d3.select("#viewDataDown").style("display","none");
	
}			

var nextClickIsADrag = false;
var nextClickIsAZoom = false;



function hoverClick(d, kind){
   log(" ** Click ** ",6)
   log("nextClickIsADrag "+nextClickIsADrag,6)
   log(d,6)
	if (!nextClickIsADrag) {
		if (typeof d != "undefined") {
			fixToolTips(true);
			var toSel=[];         
         if (kind=="Reaction" || kind=="Metabolite") {
				toSel =[d.id];
			} else if (kind=="Subsystem") {
				toSel= d.systemNodes;
			} else if (kind=="Group") {
           	toSel= d.nodes; 
         }
			
         log(toSel,6)
			
			if (d3.event.ctrlKey) {				
			   selectionToggle(toSel);				
			} else {	
				selectionDefine(toSel);
			}
			
			
			
		}   else {
			if (!nextClickIsAZoom) {		
				fixToolTips(false);		
            if (!d3.event.ctrlKey) {				
            	selectionDefine([]);
            }
			}		
		}
		//highlight(".node.hoverStyle");
	}
	nextClickIsADrag = false;
	nextClickIsAZoom = false;
}			



//!
//! Rigid groups
//!



function intersect(array1,array2) {   
   for (var i = 0; i < array1.length; i++) {
      if (array2.indexOf(array1[i])>-1) return true;
   }
   return false;
}

function recenterGroup(group) {
   log("recenterGroup",6)
   var c= quickCenter(group.nodes);
   group.nodes.forEach(function(n){
      
      n.rigidGroup.x = n.x-c.x;
      n.rigidGroup.y = n.y-c.y;
   })
   log("end recenterGroup",6)
   
}
function createRigidGroup(s){
   log("createRigidGroup",6)
   if (!s) return;
   s=uniq(s);
   if (s.length<2) return;
   if (!data.rigidGroups) {
      data.rigidGroups=[];
   }
   var group={nodes:s, rigid:true, toJSON:exportGroup};
   s.forEach(function(n){
         n.rigidGroup={}
   }) 
   var newGroup=true;
   var groupI=data.rigidGroups.length;
   data.rigidGroups.forEach(function (g,i) {
         if (newGroup && intersect(s,g.nodes)) {
            group=g;
            group.nodes=uniq(s.concat(g.nodes))
            newGroup=false;
         }
   });
   if (newGroup) data.rigidGroups.push(group);
   updateGroupIds();
   recenterGroup(group);
   updateRigidGroupsList();
   log("end createRigidGroup",6)
  
  
}
function scaleSel(scale) {
   var c=quickCenter(selection);
   selection.forEach(function(n) {
     n.x=n.px= (1-scale)*c.x+scale*n.x;
     n.y=n.py= (1-scale)*c.y+scale*n.y;       
   })
   updateView();
   
}

function killRigidGroup(group, i) {
   log("killRigidGroup",6)
   data.rigidGroups.splice(i,1);
  // console.log(i);
   group.nodes.forEach(function(n){
    //  console.log(n.rigidGroup.i);
      delete n.rigidGroup;
   })
   updateGroupIds();
   updateRigidGroupsList();
   log("killRigidGroup",6)
   
}


function killTrivialGroups() {
   for (var i = data.rigidGroups.length-1; i>=0; i--) {
      if (data.rigidGroups[i].nodes.length<2) {
         killRigidGroup(data.rigidGroups[i], i) 
      }
   }   
}
function enforceRigidGroups() {
   if (data.rigidGroups) {
     // log("enforceRigidGroups",6)
      data.rigidGroups.forEach(function (g) {
         if (g.rigid) {
            var c = quickCenter(g.nodes);
            var beforeNaN=true;
            var createNaN=false;
            g.nodes.forEach(function(n){
               beforeNaN = isNaN(n.x);
               n.x=c.x+n.rigidGroup.x;
               n.y=c.y+n.rigidGroup.y;
               createNaN=createNaN || (isNaN(n.x) && ! beforeNaN)
            })             
            if (createNaN) {
               log("created a NaN",7);
               log (g,7);
            }
         }        
      });
      //log("end enforceRigidGroups",6)
   }
   
}
function updateGroupIds() {
   log("updateGroupIds",6)
   data.rigidGroups.forEach(function(group, id) {
      group.nodes.forEach(function(n){
         n.rigidGroup.i = id
      }) 
   })
}

function updateRigidGroupsList() {
   
   
   
   var groupBoxes=
   d3.select("#selFixedGroups")
      .selectAll(".groupBox")
      .data(data.rigidGroups);
   groupBoxes.exit().remove();
   var d=
   groupBoxes.enter().append("div") 
      .attr("class","groupBox")
      .on("mouseover", function(g) {hoverOn(g, "Group") 	})
      .on("mouseout", function(g) { hoverOff()})
      .on("click", function (g) { hoverClick(g,"Group");  })
      
   d.append("span");
   d.append("input")
      .attr("class","selectRigidGroup")
      .style("margin-left","8px")
		.attr("type","checkbox")
		.property("checked",true)
		.on("change",function(g) {g.rigid = this.checked; recenterGroup(g) });
   d.append("span")
      .text("rigid")
   d.append("button")
      .text("ungroup")
      .style("float","right")
      .on("click", killRigidGroup);
   
   d.append("div")
      .style("clear","both");
              
   
   groupBoxes.select("span").text(function(d,i){return "Group "+(i+1)+": "+d.nodes.length+" nodes "})
   
   
}


function recenterRigidGroups() {
   if (data.rigidGroups) {
      data.rigidGroups.forEach(function (g) {
         recenterGroup(g) ;      
      });
   }
   
}

function firstReadRigidGroups() {
   if (data.rigidGroups) {
      data.rigidGroups.forEach(function(group, id) {
         group.nodes.forEach(function(n){
            n.rigidGroup={i:id};
         }) 
         recenterGroup(group) ;
      })      
      updateRigidGroupsList();  
   }
   log("Rigid Groups ok",1)
}

//!
//! Selections
//!
function newSelection() {	
	log("new selection",6)
/*	d3.selectAll(".node")
		.classed("highlight", get("selected"))
		.select("circle")
		.attr("stroke-width",writeNodeStrokeWidth)
		;
      */
   selection=selection.filter(function (n) {
      return n.selected= n.inFilter;
   });
      
   selnode =
    svg.select("#selcontainer")
      .selectAll(".selnode")
      .data(selection)
   selnode.exit().remove();
   selnode.enter()
      .append("g")
      .attr("class","selnode")
      .append("circle")
      .attr("cx",0)
      .attr("cy",0)
      .attr("r",70*parameters.nodeScale)
      .style("fill","#BBF")
      
   selnode
      .attr("transform",writeNodeTranslate)
      
      
    
	d3.select("#selCountMet").text(selection.filter(inFilter).filter(metabolite).length);
	d3.select("#selCountRea").text(selection.filter(inFilter).filter(reaction).length);
	if (parameters.gatherSelection) {
		updateForceStatus();
	}
}
function selectionDefine(s) {
	if (typeof s[0] == "number") 
		s=findNodes(s);
	
	selection.forEach(function(n){
		n.selected=false;
	})
	selection=s;
	selection.forEach(function(n){
		n.selected=true;
	})	
	
	newSelection();	
}
function selectionToggle(s) {
	if (typeof s[0] == "number") 
		s=findNodes(s);
	
	var adding=false;
	s.forEach(function(n) {
		if (!n.selected && n.inFilter) {
			adding=true;
			n.selected=true;	
			selection.push(n);
		}
	})
	if (!adding) {
		s.forEach(function(n) {
			n.selected=false;	
			var i=selection.indexOf(n);
			if (i>-1){
				selection.splice(i,1);
			}
		})		
	}
	//if (!adding) 
	newSelection();	
}
function selectionAdd(s) {
	if (typeof s[0] == "number") 
		s=findNodes(uniq(s));
	
	s.forEach(function(n) {
		if (!n.selected) {
			n.selected=true;	
			selection.push(n);
		}
	})
	newSelection();	
}
function selectionRemove(s) {
	if (typeof s[0] == "number") 
		s=findNodes(uniq(s));
	
	s.forEach(function(n) {
      n.selected=false;	
		var i=selection.indexOf(n);
		if (i>-1){
			selection.splice(i,1);
		}
	})	
	newSelection();	
}


function selBFS(n) {
   var queue=[n];
   var last;
   var key= "bfs";
   var bfs=0;
   
   data.nodes.forEach(function (n) {delete n[key]});
   while (queue.length) {
      var q=queue.shift();
      if (!(key in q)) {       
         q[key]=bfs;
         last=q;
         bfs++;
         findNodes(q.adjacencyList).forEach(function(r) {
           if (r.selected) queue.push(r);            
         })        
      }      
   }
   return {last:last, n:bfs};
   
}
function ySpan(s) {
   var ymin=Infinity;
   var ymax=-Infinity;
   s.forEach(function (n) {
      ymin=Math.min(ymin, n.y);
      ymax=Math.max(ymax, n.y);      
   })
   return ymax-ymin;
}
function alignSel() {
   if (selection.length<2) return;
   var c= quickCenter(selection);
   var dy=ySpan(selection)/(selection.length-1);
   
   var nDone=0;
   var start=selection[0];
   
   selection.forEach(function(n) {
      delete n.alignRank;     
   }) 
   while (start) {
      
      var bfs=selBFS(start);
            
      //iterate bfs to find a diameter   
      bfs=selBFS(bfs.last);
      bfs=selBFS(bfs.last);      
      
      start=false;
      console.log(selection.map(get("bfs"))+"")
      
      //choose between top-down or bottom-up
      var corr=0;
      selection.forEach(function(n) {
         if (("bfs" in  n)) {
            corr+=(n.y-c.y)*(n.bfs - (selection.length-1)/2)                  
         }
      })   
      
      selection.forEach(function(n) {
         if ("bfs" in  n) {
            n.alignRank=nDone + (corr>0? n.bfs : bfs.n - n.bfs-1);   
         } else if (!("alignRank" in n) && !start) {
            start=n;
         }         
      })   
      console.log(selection.map(get("alignRank"))+"")
      
      nDone+=bfs.n;
   }  
   
   
   var scale= Math.max(dy, 40*parameters.coeffArcDistance+70*parameters.nodeScale);
   
   
   selection.forEach(function(n) {
      n.y=n.py = c.y + scale*(n.alignRank - (selection.length-1)/2) ;
      n.x=n.px = c.x+Math.random();
   })  
   
   updateView();
}

d3.select("#allSel").on("click",function() {
	
	selectionDefine(data.nodes);
  
})
d3.select("#invertSel").on("click",function() {
	selection=data.nodes.filter(function(n) {return n.inFilter && (n.selected=!n.selected)});
	newSelection();
})
d3.select("#extendSel").on("click",function() {
	var ext=[];
	selection.forEach(function(n) {
		ext=ext.concat(n.adjacencyList)
	});
	selectionAdd(ext);
})
d3.select("#hideSel").on("click",function() {
	selection.forEach(function(n) {n.hideSel=true; n.selected=false;});
	selection=[];
	filterOut();
	newSelection();
});
d3.select("#unhideSel").on("click",function() {
	data.nodes.forEach(function(n) {n.hideSel=false;});
	filterOut();
	
});
d3.select("#unfreezeSel").on("click",function() {
	selection.forEach(function(n) {n.freezeSel=false; });
   node.classed("frozen",get("freezeSel"))
	updateFixed();	
});
d3.select("#freezeSel").on("click",function() {
	selection.forEach(function(n) {n.freezeSel=true; });
   node.classed("frozen",get("freezeSel"))
	updateFixed();	
});
d3.select("#keepRelativeSel").on("click",function() {
	createRigidGroup(selection.slice());
});
d3.select("#alignSel").on("click",function() {
	alignSel(selection);
});

d3.select("#scaleUpSel").on("click",function() {
	scaleSel(1.5);
   recenterRigidGroups();
});

d3.select("#scaleDownSel").on("click",function() {
	scaleSel(2/3);   
   recenterRigidGroups();
});





   /*************** PART 3  *************/
   /*   Voronoi                         */
   /*   Fixed nodes                     */
   /*   Dragging                        */
   /*                                   */
   /*                                   */
   /*                                   */
   /*************************************/   

   
//!
//!       Gathering
//!

//var gathering = {on:false, nodes:[], center: {x:0, y:0}};


function updateGathering() {
	if (parameters.gatherSelection  && selection) {
		selection.forEach(function (n) {
			n.x=n.gravityWell.x+Math.random();
			n.y=n.gravityWell.y+Math.random();      
		});
		convergeForce(1,true);
	}	
}
function startGathering(nodes) {

		gathering.on=true;

		findNodes(nodes).forEach(function (n) {
			n.x=n.gravityWell.x+Math.random();
			n.y=n.gravityWell.y+Math.random();      
		});
		convergeForce(1,true);
	
}
function stopGathering() {
	gathering.on=false;
	convergeForce(1,true);
}


//!
//!       Voronoi 
//!



var voronoi = d3.geom.voronoi()
	.x(function(d) {return d.x ; })
	.y(function(d) { return d.y ; });
	//.clipExtent([[-10, -10], [width+10, height+10]])


//creates a list of paths, representing the voronoi cells of each node
function recenterVoronoi(nodes) {
	var shapes = [];
	voronoi(nodes).forEach(function(d) {
		if ( !d.length ) return;
		var n = [];
		
		d.forEach(function(c){
			n.push([ c[0] - d.point.x, c[1] - d.point.y ]);
		});
		n.point = d.point;
		shapes.push(n);
	});
	return shapes;
}

//add a clipping region (with given class) for each node in the list
function updateVoronoi(nodeList, className) {
   var clip = container.selectAll('.clip.'+className)
			   .data( recenterVoronoi(nodeList), function(d) { return d.point.mainIndex; } );
   clip.enter().append('clipPath')
	   .attr('id', function(d) { return 'clip-'+d.point.id; })
	   .attr('class', 'clip '+className);
   clip.exit().remove()
   clip.selectAll('path').remove();
   clip.append('path')
	   .attr('d', function(d) { return 'M'+d.join(',')+'Z'; });/**/
}

//test if a node is in view (= needs to be clipped)
function needsVoronoi() {
  nodeBBox=this.getBoundingClientRect();
  return (nodeBBox.left <= svgBBox.right &&
          svgBBox.left <= nodeBBox.right &&
          nodeBBox.top <= svgBBox.bottom &&
          svgBBox.top <= nodeBBox.bottom)
}



//! Strong orientation


function updateStrongOrientation(skipConvergence) {
   //parameters.strongOrientationAngle = +d3.selectAll(".chooseStrongOrientation:checked").attr("value");
   if (parameters.orientTopDown==0) {
      d3.select("#showOrientTopDown").text("none")  
   } else if (parameters.orientTopDown<0.20) {
      d3.select("#showOrientTopDown").text("weak")  
   } else if (parameters.orientTopDown<0.35) {
      d3.select("#showOrientTopDown").text("medium")  
   } else {
      d3.select("#showOrientTopDown").text("strong")  
   }
    
   updateAxis();
   if (!skipConvergence) {
      convergeForce(1,true,true);  
   }
   updateForceStatus(); 
}


//!
//!      fixed & free nodes
//!



function setFixed(d) {
   if (d.dragging || d.freezeSel) {
      d.fixed=true;
   } else if (d.localdragging && (parameters.fluidity==2 || parameters.fluidity ==4) ) {
      d.fixed=false;
   } else if (d.neighbordragging && parameters.fluidity == 5 && metabolite(d) ) {
      d.fixed=false;
   } else if (metabolite(d)) {      
      d.fixed= parameters.fluidity==0 || ((parameters.fluidity==3 || parameters.fluidity==4 || parameters.fluidity==5)  && !d.cofactor);
   } else {
      d.fixed= parameters.fluidity<=2;
   }
   
  // d.fixed = (d.group=="metabolite" ? parameters.fluidity<1 : parameters.fluidity<3);   
   return d.fixed;
}
//d3.selectAll(".chooseFix").on("click", updateFixed());


function updateFixed() {
    //parameters.fluidity = d3.selectAll(".chooseFix:checked").attr("value")*1;
  
    d3.selectAll(".node")
      .classed("fixed", setFixed);
    
	updateForceStatus();
}

function updateForceStatus() {
	
    if (typeof force != 'undefined') {	   
	
        if (parameters.fluidity == 0) {
            force.stop();
        } else {      
	//		if (typeof force.alpha() == "undefined") {
				force.start();	
	//		} else {
	//			force.resume();
	//		}
		}
	} 
	
}



		
//!
//!     node drag (overriding the one from force to avoid conflict with zoom)
//!



var drag = d3.behavior.drag()
		.origin(function(d) { return d; })
		.on("dragstart", dragstarted)
		.on("drag", dragged)
		.on("dragend", dragended);

  

var draglist=[];
function dragstarted(d) {
   d3.event.sourceEvent.stopPropagation();
     
   d3.select(this).classed("dragging", true);
   
	trackZoom = [];  
   draglist=[d];
	if (parameters.selectionDrag && selection && d.selected) 
		draglist=selection;
   if (d.rigidGroup && data.rigidGroups[d.rigidGroup.i].rigid) {
      draglist=uniq(draglist.concat(data.rigidGroups[d.rigidGroup.i].nodes));
   }
	draglist.forEach (function(d) {
		
	 d.dragging=true;
	 d.distance[locality].forEach(function(x) {
          data.nodes[x].localdragging=true;
     });
     d.distance[1].forEach(function(x) {
          data.nodes[x].neighbordragging=true;
     });
	})
   updateFixed();	
}

function dragged(d) {
	draglist.forEach (function(d) {
		
     d.x+=d3.event.dx;
     d.y+=d3.event.dy;
     d.px+=d3.event.dx;
     d.py+=d3.event.dy;
   })
	if (parameters.fluidity) {
      force.tick();
   } else {
      updateView();
   }  
   nextClickIsADrag = true;
      
     //d3.select(this).attr("transform", "translate("+d.x+","+d.y+")");         
}

function dragended(d) {
	if (parameters.selectionDrag) {
		node.classed("dragging", false)
			.each(function(d) {
			   d.dragging=false;
				d.localdragging=false;
				d.neighbordragging=false;	
			});
     
	}	else {
     d3.select(this).classed("dragging", false);
     d.dragging=false;
     d.distance[locality].forEach(function(x) {
         data.nodes[x].localdragging=false;
         data.nodes[x].neighbordragging=false;
     });
	}
     updateFixed();		    
     force.tick();
}
   
		
		





   /*************** PART 4  *************/
   /*   Layout                          */
   /*    - Force                        */
   /*    - Settling                     */
   /*    - Tick                         */
   /*   Zoom                            */
   /*   Resize                          */
   /*                                   */
   /*************************************/   

//!
//!        Force layout 
//!


var force = d3.layout.force()
  .gravity(0) 
  .theta(1.0)
  .friction(0.7);

function setupForceLayout(force,  n,  rounds) {

	
    
   if (!data.processed) {     
	    data.nodes.forEach(function(d, i) { 
         d.x =  d.gravityWell.x + Math.cos(i/n*6.28)*(d.ccdepth+8*d.gravityStrength-4)*Math.sqrt(n); 
         d.y =  d.gravityWell.y + Math.sin(i/n*6.28)*(d.ccdepth+8*d.gravityStrength-4)*Math.sqrt(n); 
         d.px=d.x;
         d.py=d.y;
       });
   }  ; 
   
	//give arcs and nodes to force layout 
   force.nodes(data.nodes)
         .links(data.links.concat(data.extraLinks))
         .linkDistance(writeArcDistance)
         .linkStrength(writeArcStrength)
         .charge(writeNodeCharge);
	log("node 0 x: "+data.nodes[0].x,2);
   log("NaNs : "+data.nodes.map(get("x")).filter(isNaN).length,7);
    convergeForce(rounds,true)
    
	log("node 0 x: "+data.nodes[0].x,2);
    
    updateAxis()	             
    log("Force setup: ok",1);	
}

//repulsion force between a reaction and a linked node
function applyRepulsion(r,moveX,moveY, alpha) {
	return  function(n) { 
				var strongerForce=(n.cofactor?1:0.1)
				if (!n.fixed) {
					var dx=n.x-r.x,
                   dy=n.y-r.y,
                  scal=(dx*r.axis.x+ dy*r.axis.y)/(Math.sqrt(moveX*moveX+moveY*moveY)+1);
					if (scal<0.5) {
						strongerForce*=Math.min(10, (0.5-scal)*10); //force is 10x stronger if node is on the "wrong side" of the reaction
					}					
               strongerForce*=alpha;
					n.x+= moveX*strongerForce;
					n.y+= moveY*strongerForce;  
				} 
				if (n.inFilter) {  
					hasMoved=1;
				}
			 }
	
}

var last = {x:0, y:0};
function logM(s) {
  //ignored   
}

force.on('tick', function(e) {
	log("tick",2);
	tickcount++;
  //the force layout already took care of
  // - charge repulsion
  // - links attraction
	
  //specific forces:	
  // - custom gravity:
  //   (pull towards individual gravity wells, depending on CC and compartments)
   
   logM("start: "+e.alpha);
   var k = 0.2 * e.alpha;         
   data.nodes.forEach(function(d){
      if (!d.fixed && d.inFilter) {
		strength=d.gravityStrength; 
		var toX=d.gravityWell.x;		
		if (parameters.gatherSelection) { //gathering.on) {
			if (d.selected) 
				//strength*=2;
				toX+=1000;
			else
				toX-=1000;
				//strength*=0.3;
		}
      var dx=(toX - d.x), dy =   (d.gravityWell.y - d.y);
		d.x+=dx*k*strength;
		d.y+=dy*k*strength; 
		
        var z= dx*dx+dy*dy ;
        if (z>d.comfortDistanceSquare) {
           d.x+=dx*k*strength*parameters.coeffGravityFar*(z-d.comfortDistanceSquare)/z;
           d.y+=dy*k*strength*parameters.coeffGravityFar*(z-d.comfortDistanceSquare)/z;
        }
		
		/*
		if (gathering.on) {
			var dx=(gathering.center.x - d.x), dy =   (gathering.center.y - d.y);
			if (gathering.nodes.indexOf(d.id) ==-1) {
				dx*=-50*parameters.coeffArcDistance/Math.sqrt(dx*dx+dy*dy);
				dy*=-50*parameters.coeffArcDistance/Math.sqrt(dx*dx+dy*dy);
			} else {
				dx*=0.5;
				dy*=0.5;
			}
			d.x+=dx*k;
			d.y+=dy*k; 			
		}*/
		
		
	  }		      
   });
   
   logM("after gravity");
   
   // - custom repulsion:
   //   (each reaction push its sources and targets in opposite directions)
          
   if (parameters.coeffReactionRepulsion) {	
	   data.nodes.forEach(function(r){				
         if (reaction(r) && r.inFilter) {
            var moveX = r.axis.x;
            var moveY = r.axis.y;
           
            moveX*=  parameters.coeffReactionRepulsion*20; 
            moveY*=  parameters.coeffReactionRepulsion*20; 


            findNodes(r.outAdjacencyList).forEach(applyRepulsion(r, moveX,moveY, e.alpha))	
            moveX*=-1;
            moveY*=-1;
            findNodes(r.inAdjacencyList).forEach(applyRepulsion(r, moveX,moveY, e.alpha))				 	
		  }		      
	   });
   }
   
   logM("after repulsion");
   
   // - arc orientation:
   //   (each reaction pulls its sources and targets towards 2 specific points)
   if (parameters.strongOrientationStrength) {
      var k=  Math.min(e.alpha*parameters.strongOrientationStrength,1);
      coeffArcDistance=6;
      data.links.forEach(function(l){			
         if (l.inFilter) {
            
            var wishX=l.reaction.x + (l.outward? +1 : -1) * l.arcDistance * l.reaction.axis.x*coeffArcDistance;            
            var wishY=l.reaction.y + (l.outward? +1 : -1) * l.arcDistance * l.reaction.axis.y*coeffArcDistance;
            var str= k*(l.cofactor?0.3:1)/(l.reaction["strong"+(l.outward?"Out":"In")+"Degree"] +1);
            
            if (!l.metabolite.fixed) {
               l.metabolite.x+=(wishX-l.metabolite.x)*str;
               l.metabolite.y+=(wishY-l.metabolite.y)*str;
            }
            if (!l.cofactor && !l.reaction.fixed) {
               l.reaction.x-=(wishX-l.metabolite.x)*str/5;
               l.reaction.y-=(wishY-l.metabolite.y)*str/5;
            }
            
            
         }
      })
	
   
   }
   
   logM("enforceRigidGroups");
   
  /*data.nodes.forEach(function(n){
       if (isNaN(n.x) || isNaN(n.y)) {
          n.x=n.px;
          n.y=n.py;
       }  
       n.px=n.x;
       n.py=n.y;
   })
 */     
   enforceRigidGroups();
   
   /*data.nodes.forEach(function(n){
       if (isNaN(n.x) || isNaN(n.y)) {
          log("Found NaN",7)
          n.x=n.px;
          n.y=n.py;
          //log(n,7)
       }  
   })*/
      
   var NaNs=  data.nodes.map(get("x")).filter(isNaN).length;
   if (NaNs) log("NaNs : "+NaNs,7);
   logM("end");
   log("",4);
	
	// update the view if ready	
	if (enableViewUpdate && tickcount%3==0)
		zoomOnNodes(trackZoom);
	updateView();     
	

});


//!
//! Settling functions
//!

function convergeForce(rounds, keepFixedNodes) {
    if (rounds>0) {
    //  go=function() {
         d3.select("#layoutInProgress").style("display","block");
         enableViewUpdate=false;
         var moveNew=keepFixedNodes == "new";
         if (!keepFixedNodes || moveNew) {
            d3.selectAll(".node")
             .classed("fixed",function(d) {return (d.fixed=(d.fixed && moveNew && d.inFilter && d.wasInFilter)); } );
         }
         log("Reset view (run converge)",1)
         force.start();
         force.alpha(0.75);
         for (var j = rounds; j > 0; --j) {
            for (var i = nNodes; i > 0; --i) {
              
               if (i%2==0) updateAxis();
               force.alpha(Math.max(0.5,force.alpha()));
               force.tick(); 
            }
         }  
         for (var i = 100; i > 0; --i) {              
            if (i%4==0) updateAxis();
            force.tick(); 
         }
         force.stop();
         enableViewUpdate=true; 
         updateFixed();  
         updateView();    
         d3.select("#layoutInProgress").style("display","none");       
    /*  }
      if (parallel) {         
        d3.timer(go, 1);
      } else {
         go();
      }*/
	} else {
      updateView();     
   }
}

d3.select("#shuffle").on("click", shuffleEverything);
d3.select("#coolDown").on("click", function() {convergeForce(1,true)});

function shuffleEverything() {
   extend=500;
   data.nodes.forEach(function (d) {
      dx=Math.random()*2*extend - extend; 
      dy=Math.random()*2*extend - extend;
      findNodes(d.extendedAdjacencyList).forEach(function(n) {
         d.x+=dx     
         d.y+=dy
      });
   })
   convergeForce(4, false,true);

}


//!
//!   Update View after coordinate change
//!


function writeNodeTranslate(d) { 
   return  'translate('+d.x+','+d.y+')'; 
}

function writeNodeRotate(r) { 
   if (!reaction(r)) {
      return "";
   }  
   return " matrix("+ r.axis.showX + ","+
                      (r.axis.showY) + ","+
                      (-r.axis.showY) + ","+
                      r.axis.showX + ",0,0)";   
}

function updateView(nodesHaveNotMoved) {
	log("updateView "+nodesHaveNotMoved,2)
   if (typeof node != "undefined" && enableViewUpdate) {
		if (!nodesHaveNotMoved) {
			updateAxis();
			log("*update node positions",3)
			node.attr('transform', writeNodeTranslate);
			node.select(".rotateBox").attr('transform', writeNodeRotate);
         
         selnode.attr('transform', writeNodeTranslate);

			log("*update link endpoints",3)
			link.attr('d', writeArcPath );
         
			
		}		
      //voronoi update:
      //check if nodes are big enough (otherwise: no voronoi = voronoi on empty list)
	  
		var tooClose=closeZoom(parameters.voronoiThreshold);
		
		log("*close for voronoi: "+tooClose,2)
		if (parameters.useNodeVoronoi) {
			updateVoronoi(tooClose? metabolitenode.filter(needsVoronoi).data() : [], 'nodeclip')  
		}
		if (parameters.useEdgeVoronoi) {
			updateVoronoi(tooClose? reactionnode.filter(needsVoronoi).data() : [], 'reacclip')  
		}
   }
   log("updateView: done",2);
}

//!
//!     zoom and pan 
//!



var zoom = d3.behavior.zoom()
		.scaleExtent([0.05, 20])
		.on("zoom", zoomed);
function zoomed() {
	trackZoom = [];
   d3.select(".drawarea").attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
   updateView(true); //the view changes, but nodes relative coordinates haven't changed (possibly some voronoi to update)
   //d3.select(".drawarea").attr("transform", "translate(" + zoom.translate + ")scale(" + d3.event.scale + ")");
   nextClickIsAZoom=true;
   displayShadows(closeZoom(parameters.shadowThreshold));

}

function displayShadows(display) {
      d3.select(".mainShadowFilter").attr("id", display? "shadow": "noShadow");
      d3.select(".uselessShadowFilter").attr("id", display? "noShadow": "shadow");
}
		

function closeZoom(threshold) {
   return !metabolitenode.empty()  && 
		      (metabolitenode.node().getBoundingClientRect().width>threshold);
}
		
			
		
		
//		
//  resize as window
//


window.onresize = updateWindow;
var svgBBox={};

function updateWindow(){
	var w = window,
     d = document,
     e = d.documentElement,
     g = d.getElementsByTagName('body')[0];
	
   width = w.innerWidth || e.clientWidth || g.clientWidth;
   height = w.innerHeight|| e.clientHeight|| g.clientHeight;
   width -= windowMarginHor;
	height-=  windowMarginVer;
   svg.attr("width", width).attr("height", height);
   svg.selectAll(".background").attr("width", width).attr("height", height);
   svgBBox=svg.node().getBoundingClientRect();   
}


//link surrounding effect

function addShadowDefinitionNone(svg, id, className) {
// create filter with id #drop-shadow
	// height=130% so that the shadow is not clipped
	defs.append("filter")
		.attr("id", id)
		.attr("class",className)
		.attr("height", "130%")
	  .append("feMerge")	 
     .append("feMergeNode")
		.attr("in", "SourceGraphic");
		
}

//link surrounding effect


function addCustomShadowDefinition(svg, id,className, colorMatrix) {

	 
	// create filter with id #drop-shadow
	// height=130% so that the shadow is not clipped
	var filter = defs.append("filter")
		.attr("id", id)
		.attr("class",className)
		.attr("height", "130%");
	 
	// SourceAlpha refers to opacity of graphic that this filter will be applied to
	// convolve that with a Gaussian with standard deviation 3 and store result
	// in blur
	filter.append("feGaussianBlur")
		//.attr("in", "SourceAlpha")
		.attr("stdDeviation", 1)
		.attr("result", "blur");
	 
	 
	// translate output of Gaussian blur to the right and downwards with 2px
	// store result in offsetBlur
	filter.append("feOffset")
		.attr("in", "blur")
	//	.attr("dx", 5)
	//	.attr("dy", 5)
		.attr("result", "offsetBlur");
	
	filter.append("feColorMatrix")
		.attr("in", "offsetBlur")
		.attr("type", "saturate")
//		.attr("values","100 0 0 0 100 0 100 0 0 100 0 0 100 100 0 0 0 0 1 0" )
		.attr("values","1" )
		.attr("result", "saturateOut");	
	filter.append("feColorMatrix")
		.attr("in", "saturateOut")
		.attr("type", "matrix")
		.attr("values",colorMatrix )
		.attr("result", "matrixOut");
	
	  
	// overlay original SourceGraphic over translated blurred opacity by using
	// feMerge filter. Order of specifying inputs is important!
	var feMerge = filter.append("feMerge");
	 
	feMerge.append("feMergeNode")
		.attr("in", "matrixOut")
	feMerge.append("feMergeNode")
		.attr("in", "SourceGraphic");
}	 

function addShadowDefinition(svg, id,className) {
	addCustomShadowDefinition(svg, id,className, 
			"2 1 1 1 0 \
              1 2 1 1 0 \
              1 1 2 1 0 \
              1 1 1 10 0") 
}

function addShadowDefinitionHighlight(svg, id,className) {
	addCustomShadowDefinition(svg, id,className, 
			"3 3 3 3 0 \
              3 3 3 3 0 \
              0 0 0 0 0 \
              1 1 1 10 0") 
}	/*
	// filters go in defs element
	var defs = svg.append("defs");
	 
	// create filter with id #drop-shadow
	// height=130% so that the shadow is not clipped
	var filter = defs.append("filter")
		.attr("id", id)
		.attr("class",className)
		.attr("height", "130%");
	 
	// SourceAlpha refers to opacity of graphic that this filter will be applied to
	// convolve that with a Gaussian with standard deviation 3 and store result
	// in blur
	filter.append("feGaussianBlur")
		//.attr("in", "SourceAlpha")
		.attr("stdDeviation", 1)
		.attr("result", "blur");
	 
	 
	// translate output of Gaussian blur to the right and downwards with 2px
	// store result in offsetBlur
	filter.append("feOffset")
		.attr("in", "blur")
	//	.attr("dx", 5)
	//	.attr("dy", 5)
		.attr("result", "offsetBlur");
	
	filter.append("feColorMatrix")
		.attr("in", "blur")
		.attr("type", "saturate")
//		.attr("values","100 0 0 0 100 0 100 0 0 100 0 0 100 100 0 0 0 0 1 0" )
		.attr("values","1" )
		.attr("result", "saturateOut");	
	filter.append("feColorMatrix")
		.attr("in", "saturateOut")
		.attr("type", "matrix")
		.attr("values","3 3 3 3 0 \
              3 3 3 3 0 \
              0 0 0 0 0 \
              1 1 1 10 0" )
		.attr("result", "matrixOut");
	
	  
	// overlay original SourceGraphic over translated blurred opacity by using
	// feMerge filter. Order of specifying inputs is important!
	var feMerge = filter.append("feMerge");
	 
	feMerge.append("feMergeNode")
		.attr("in", "matrixOut")
	feMerge.append("feMergeNode")
		.attr("in", "SourceGraphic");
	 
}*/
			
   /*****  PART 4 3/4  ******************/
   /*                                   */
   /*  Parse text file                  */
   /*                                   */
   /*                                   */
   /*************************************/

function HyperToBipartite( HJ) {
   data.nodes=[];
   data.links=[];
   
   HJ.nodes.forEach(function(n) {
      var nn = {
        group:"node",
        name:n.name
        }       
      data.nodes.push(nn)});
   HJ.hyperedges.forEach(function(h) {
      var nh = {
        group:"hyperarc",
        name:h.name
        }     
      if ("system" in h) nh.subsystem=h.system;  
      var i=data.nodes.push(nh)-1;
      h.sources.forEach(function(s) {
        var l={source: s, target:i};
         if ("system" in h) l.subsystem=h.system;  
        data.links.push(l);
      });
      h.targets.forEach(function(t) {
         var l={source: i, target:t};
         if ("system" in h) l.subsystem=h.system;  
         data.links.push(l);
      }); 
   })  
   if ("systems" in HJ) data.systems=HJ.systems;  
   return data; 
}

function cleanName(s) {
  return s.replace(/\W/g,"").trim();  
}
function notEmpty(s) {
   return s!="";
}
function parsePart(str) {
  return str.split('+').map(cleanName).filter(notEmpty); 
}
function addToMaps(names, map, array) {
  
  names.forEach(function(n){
    if (typeof map[n] == "undefined") {      
      map[n]=array.push({name:n}) - 1;
    }
  })
}

function txtToHyper(file) {
	var data= {nodes:[], hyperedges: [], systems:[]};
	var nodeMap={};   
	var systemMap={};
	ECount = 0;   
	NCount = 0;
	file.split('\n').forEach(function (line) {
		if (/->/.test(line)) {
			var h={};
		   h.name=line;
		   system="";
		   if (/:/.test(line)) {
   			parts=line.split(':');
			   line=parts[1];
			   
   			metadata=parts[0].split(',').map(cleanName);
   			h.name=metadata[0];
   			if (metadata.length>=2) system=metadata[1];
   			if (system!="" && typeof systemMap[system] == "undefined") {
   			   systemMap[system]=1;
   			   data.systems.push({"id":system});
   			}			   
   			if (system!="") {
   			   h.system=system;
   			}			   
   			console.log("system : "+system+"name :"+name);
			}
			
			parts=line.split('->').map(parsePart);

			addToMaps( parts[0], nodeMap, data.nodes);
			addToMaps( parts[1], nodeMap, data.nodes);
		
			h.sources=[];
			h.targets=[];
			parts[0].forEach(function(n) {
				h.sources.push(nodeMap[n]);
			});
			parts[1].forEach(function(n) {
				h.targets.push(nodeMap[n]);
			});
				  
			data.hyperedges.push(h);
		}           

	});   

	return data;
}

function testForTextFile(){
   if ((typeof data.nodes == "undefined") && (typeof data.text =="string")) {
      log("Input of simple Hypergraph",1);
      data = HyperToBipartite( txtToHyper(data.text));   
   }
   
}
//var test=" a +b -> c\nb+c -> d \n a->d \n ->e";
//log(JSON.stringify(HyperToBipartite(txtToHyper(test))));


   /*****  PART 5  **********************/
   /*                                   */
   /*  MAIN : open data                 */
   /*                                   */
   /*                                   */
   /*************************************/


//
// *************** Main stuff ! **************
//



//start dimension (before resize)
var width = 800,
  height = 600;
  windowMarginVer = 50
  windowMarginHor = 10
  
  
//svg object 
var svg = d3.select("#svgHolder").append("svg")
	  .attr("pointer-events", "all");


// set dimensions as the window
updateWindow();

var defs = svg.append("defs");

addShadowDefinition(svg,"shadow", "mainShadowFilter");
addShadowDefinitionHighlight(svg,"shadowYellow", "yellowShadowFilter");
 addShadowDefinitionNone(svg,"noShadow", "uselessShadowFilter");
 

//background rectangle	
svg.append("rect")
	   
	  .attr("class", "background")
	  .on("click", function(){
	         hoverClick()
   	      //filter(false);
	      })
		.attr("fill","#FFF") //FEC 
	  .attr("width", width)
	  .attr("height", height);
	  
// <g> containers for whole figure 
	  
   var parent=svg.append("g")
         		.attr("class", "drawarea");
  
	container = parent.append("g")
	container.attr("transform","translate("+(width/2)+","+(height/2)+")scale(0.4)");	
	container.append("g").attr("id","selcontainer")	
	container.append("g").attr("id","linkcontainer")	
   container.append("g").attr("id","nodecontainer")
  
		  
   // the whole svg triggers the zoom & pan 
   zoom(svg);



function openData(json) {

	/**********************/
	/** process raw data **/
	data=json;   

	testForTextFile();	

	importOlderVersions();
	importRawData();

	//read the graph, populate source and targets, filter out some nodes
	checkData();

	//read the "parameters" field of data and the "rawParameters" object, and import their values
	parseInputParameters();

	//compute local neighborhood and connected component decomposition
	neighborhood();

	//add fields to nodes, arcs and systems
	console.log(data.nodes.map(get("raw", "compartment")))
	console.log(data.nodes.map(get("compartment")))
	console.log(data.nodes.map(get("compartment")).map(get("name")))
	firstReadNodes();
	firstReadArcs();
	console.log(data.nodes.map(get("raw", "compartment")))
	console.log(data.nodes.map(get("compartment")))
	console.log(data.nodes.map(get("compartment")).map(get("name")))
	firstReadSystems();
	console.log(data.nodes.map(get("raw", "compartment")))
	console.log(data.nodes.map(get("compartment")))
	console.log(data.nodes.map(get("compartment")).map(get("name")))
	firstReadCompartments();
	console.log(data.nodes.map(get("raw", "compartment")))
	console.log(data.nodes.map(get("compartment")))
	console.log(data.nodes.map(get("compartment")).map(get("name")))
	firstReadRigidGroups();

	//create extra links between substratres/products of a reaction
	addBrotherLinks();

	//compute simple graph properties
	computeOutsiders();
	computeCCs();

	//compute median value of reactions
	computeMedianValue();
	//give a gravity well to each node
   updateDynamicsCoeff();
   updateStrongOrientation(true);

	 
	//give initial positions to nodes, and run several rounds of force ticks.
	setupForceLayout(force,  nNodes, data.processed?0 : parameters.rounds);  

	
	/*******************/
	/** show the data **/    
		 
		 
	// side panel: system boxes      
	fillSystemBox();   
         
	// side panel: checkbox values
	updateFilter( true); 
	updateShowCofactors(true);
   
   
	// nodes and links (based on filter)   
	filterOut(data.processed);
	
	updateFixed();
	
	updateView();
	
	//mark data as processed
	data.processed=true;	

}
  
function changeParameterValue(p,newValue) {
   log("set parameter " +p+"="+newValue,2);
   parameters[p] = newValue;
   if (setterFunction[p]) {
		setterFunction[p](newValue);
	}/* else if (p=="fluidity") {
      d3.selectAll(".chooseFix").each(function(x) {
            if (this.value == newValue) this.checked=true;
      })
   } else if (p=="showCofactors") {   
       d3.select("#showCofactors").node().checked = newValue;
   } else if (p=="compartmentPlacement") {   
       d3.select("#chooseCompartment").node().checked = newValue;
   } else if (p=="compartmentColor") {   
       d3.select("#chooseColorCompartment").node().checked = newValue;
   } else if (p=="strongOrientationAngle") {
		d3.selectAll(".chooseStrongOrientation").each(function(x) {
     //       if (this.value == newValue) this.checked=true;
      })	
	} else if (p=="gatherSelection") {   
       d3.select("#gatherSelection").node().checked = newValue;
   } */
}
function parseInputParameters() {
   if (typeof data.parameters != "undefined") {
      for (var p in data.parameters) {
            changeParameterValue(p, data.parameters[p]);
      }
   } 
   
   if (typeof rawParameters != "undefined") {
      for (var p in rawParameters) {
         if (rawParameters[p]!="") {
            changeParameterValue(p, rawParameters[p]);
         }
      }
   }
   data.parameters=parameters;   
}


var deleteJSONKeys=["original", "outward", "metabolite", "reaction", "adjacencyList", "inAdjacencyList","outAdjacencyList", "distance","extendedAdjacencyList","ccdepth","connectedcomponent","keep","extraLinks","strokewidth","systemNodes","adjacentLinks"];


function jsonReplace(key, value) {
   
	if (deleteJSONKeys.indexOf(key) >-1) {
		return;
	}
	if ((key=="source" || key=="target") && (typeof value == "object")) {   
		return value.mainIndex;
	}  
	
	return value;  
}
function exportJson() {  
   return JSON.stringify(data, jsonReplace,' ');
}

/** end:  get the data and start the process */
 
if (typeof rawData == "undefined") {
   log("Reading example",0)
   if (parameters.textInput) {
      json= {};
      d3.text(parameters.textInput,"text/plain", function(s) {
         openData({text:s})
      });
   } else {
      d3.json(parameters.data, openData);
   }
} else {
   log("Reading uploaded file",1)
   openData(rawData);
}




   /*****  PART 6  **********************/
   /*                                   */
   /*  DEMO                             */
   /*                                   */
   /*                                   */
   /*************************************/

  
if (parameters.demo) {
   var demoTour=[
    [""],  
    ["DINGHY",
     '<img style="float:none;position:relative; height:180px" src="images/DinghyLogo.png"><br>'
	 + (parameters.demo=="jobim"?
	 '<div style="margin:5px"><b>JOBIM 2015</b></div>\
	 <div style="margin:5px">Laurent&nbsp;BULTEAU<br> Alice&nbsp;JULIEN-LAFERRIERE <br>Vincent&nbsp;LACROIX <br>Delphine&nbsp;PARROT<br> Marie-France&nbsp;SAGOT</div><div style="margin:5px">LBBE; Université Lyon 1; INRIA</div>'
    :
	  '<i>Demonstration Tour</i>'
     )
	 + '<div class="info">This window presents the main features of Dinghy.<br> Click here or use the arrow keys to go through the presentation</div>',     
     "Dynamic Interactive Navigator for General Hypergraphs in Biology"],
    ["Display metabolic networks",
     "Or any oriented hypergraph",
     "Metabolites / nodes => disks",
     "Reactions  / hyperedges => links and arrows"],     
    ["Some Features",
     'Highlight an element and all neighbors\
     <div class="info">Move your mouse over any metabolite:<br> neighboring reactions are highlighted as well as the substrates (resp. products) which can be used to produce (resp., which can be produced by) this metabolite</div>',
     'Click for meta-data\
     <div class="info">Clicking on a node displays the available meta-data</div>',
     'Easy navigation\
     <div class="info">Zoom with the mouse-wheel, <br>drag and drop the background to move</div>'],
    ["Dynamic node placement",
     "Drag and drop nodes",
     "The graph rearranges itself",
     'Smart overlap\
     <div class="info">If two nodes are too close to each other, by zooming on them, then one should not cover the other</div>'],
    ["Use meta-data",
     'Cofactors\
     <div class="info">Known cofactors are uncoupled and do not influence the layout</div>',
     'Value associated to each reaction\
     <div class="info">Determines edge thickness</div>',
     'Metabolic pathway\
     <div class="info">Determines edge colors</div>',
     "Display formula, charge, compartments, ..."],
    ["Force layout",
     'Link attraction\
     <div class="info">Nodes are attracted to their neighbors, up to a fixed distance</div>',
     'Node repulsion\
     <div class="info">Nodes repulse each other, to avoid overlap</div>',
     'Gravity\
     <div class="info">Nodes are clustered around centers of gravity, depending on their connected components</div>',
     'Separate sources and targets\
     <div class="info">Substracts and products of a reaction try to be separated, one group on either side.</div>',
     'Top-down orientation\
     <div class="info">The reactions try to rotate so that substrates are above and products below</div>'],
    ["Settings Menus",
     '<img src="images/btn-fluidity.svg">Node Dynamics\
     <div class="info">Choose a degree between automatic placement of the nodes and precise positionning by the user</div>',
     '<img src="images/btn-fluidity.svg">Global Layout\
     <div class="info">Set global positionning parameters: strength of gravity, repulsion, or arc length<br>\
       <b>Shuffle the nodes</b> will randomly move the nodes to reach another stable state<br>\
       <b>Graph cool down</b> will help the graph reach a stable position more quickly</div>',
     '<img src="images/btn-fluidity.svg">Arc Orientation\
     <div class="info">Force arcs to follow fixed directions<br>\
       Set importance of the top-down orientation</div>',
     '<img src="images/btn-filter.svg">Hide cofactors',
     '<img src="images/btn-filter.svg">Hide reactions with small value',
     '<img src="images/btn-selection.svg">Selections\
     <div class="info">Select several nodes to move them as one element<br>\
       Selections can be hidden, or frozen in place</div>',
     '<img src="images/btn-selection.svg">Groups\
     <div class="info">Selection can be saved into a group<br>\
       Nodes in a "rigid" group keep their relative position<br>\
       Any node can belong to only 1 group</div>',     
     '<img src="images/btn-search.svg">Path search\
     <div class="info">Give start and end nodes, this feature gives all directed paths</div>',     
     '<img src="images/btn-search.svg">Search for a specific element\
     <div class="info">Use name or id. <br> Hiden elements appear in grey in the results<br> Results can be used as start or end nodes.</div>',
     '<img src="images/btn-photo.svg">Download a still snapshot\
     <div class="info">In svg format (vectorial, can be converted to any other image format)</div>',
     '<img src="images/btn-photo.svg">Save the current view\
     <div class="info">In json format, which can be uploaded to be seen again</div>',
     '<img src="images/btn-pathways.svg">Pathways:<br> highlight, select and toggle display\
     <div class="info">Provided the information is available in the SBML (look for "PATHWAY" field in the species notes</div>',
     '<img src="images/btn-compartments.svg">Miscellaneous \
     <div class="info">Set size of nodes <br> Highlight compartments, toggle compartment separation</div>'],
    ["Other settings (home page)",
     "Coefficients for gravity, repulsion, etc.",
     "Node and arc scales",
     "Speed-up settings"],
    ["Based on Javascript",
     "On-line tool",
     "Works with modern browsers",
     "Uses a python script for pre-processing",
     'Works well with up to ~100,150 reactions\
     <div class="info">Depending on the computer'+"'"+'s efficiency</div>'],
    ["Pre-processing biological network",
     'Input:<br> SBML file of the system\
     <div class="info">Representing the whole metabolic system, and including most of the meta-data</div>',
     'Input:<br> Reactions of interest\
     <div class="info">Text file containing the list of reactions to display (1 per line, with associated values if available)</div>',
     'Input:<br> Nodes of interest\
     <div class="info">List of nodes to be highlighted (dark red nodes)</div>',
     'Input:<br> Cofactors list',
     "Output:<br> .json file, to upload"],
    ["Simple hypergraphs",
     "Direct input: no pre-processing",
     "a + b -> c + d <br> a + c -> e <br> e->d"],    
    ["Credits",
     'Developped in the <a href="https://team.inria.fr/erable/en/">Erable</a> Inria Team',
     "By L. Bulteau, D. Parrot, A. Julien-Laferrière", 
     'Uses the <a href="http://d3js.org">d3.js</a> library', 
     'Contact: l.bulteau [at] gmail.com'],
     ['Thank you for taking the tour!<br>\
     <img style="float:none; position:relative; height:180px" src="images/DinghyLogo.png">',
      'dinghy.gforge.inria.fr']
     ];

   var demoStep=1, demoSubstep=0;
   function demoHighlight(id){
     d3.select(id).style("opacity", 1).style("background-color", "#ddf").transition().style("opacity", 1).style("background-color", "#fff");
   }
   function showDemo() {
         d3.select("#demo1").html(demoTour[demoStep][0])
         d3.select("#demo2").html(demoSubstep>0 ? demoTour[demoStep][demoSubstep] : "");      
   }
   function demoProceed() {
      if (demoStep<demoTour.length -1 || demoSubstep < demoTour[demoStep].length-1) {
         demoSubstep++;
         if (demoSubstep >= demoTour[demoStep].length) {
            demoStep++;
            demoSubstep=0;
            demoHighlight("#demo1")
         }
         showDemo();
      }
   }
   function demoReturn() {
      if (demoStep >1 ||demoSubstep>1) {
         demoSubstep--;
         if (demoSubstep<0) {
            demoStep--;
            demoSubstep=demoTour[demoStep].length-1;
         }
         showDemo();
      }
   }
   function demoProceedQuick() {
      if (demoStep<demoTour.length -1 ) {
         demoStep++;
         demoHighlight("#demo1")
         demoSubstep=0;
         showDemo();
      }
   }
   function demoReturnQuick() {
      if (demoStep >1) {
         demoStep--;
         demoHighlight("#demo1")
         demoSubstep=(demoStep==1?1:0);
         showDemo();
      }
   }
   document.onkeydown = function(e) { 
         if (e.keyCode==37) demoReturn();
         if (e.keyCode==33) demoReturnQuick();
         if (e.keyCode==34) demoProceedQuick();
         if (e.keyCode==39 || e.keyCode==32) demoProceed()
         };
   d3.select("#demoTour").style("display","block").on("click", demoProceed);      
   
   demoProceed();
} 
     

//!
//!  Chemins
//!
function chemins(x,y) // determine source and target of interest
{ 
    var pool=[[x]]; // all pathways not finished (waiting queue for incomplete paths)
    result=[]; // list of all pathways are possible from S to T
    while(pool.length>0)// presence of one data at least (in "pool")
    { 
    chemin=pool.shift(); // remove the first element present in "pool" and place it in "chemin"
    dernier=chemin[chemin.length-1]; // take the last element present in "chemin" and put it on "dernier"
    listadjacent=data.nodes[dernier].nextStep;// ensemble of ["reactions", "nodes" ???] connected to "dernier"
    
    listadjacent.forEach(function(id) // call function for each element present in "listadjacent"
    {   
        
        if (data.nodes[id].inFilter ==1)// presence in the windows visualization
        {    
            if (chemin.indexOf(id)==-1 || (id ==x && (x==y)))  // if "id" isn't present in "chemin" or "id"= S and  S=T
            { 
                newchemin=chemin.concat([id]); // add "id" in "newchemin" list
                if (id==y)// id = Target
                { 
                    result.push(newchemin);// put "new chemin" list in "result" list                             
                } 
                
                else 
                {
                    pool.push(newchemin);// if not, put "new chemin" list in "pool"
                } //end if else
            }
        }
    })
    }        
    return result;    // return "result" list
    
    
    
}



/* Research by name or id*/
function researchname (name)
{
    var tablelength=data.nodes.length;
    for (var i=0; i<tablelength; ++i)
    {
        var namenodei=data.nodes[i].name;
        if (namenodei == name || data.nodes[i].id == name)
        {
            return i;
        }
    }
return -1;
}    

	

